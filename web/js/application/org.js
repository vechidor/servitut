$(".add_employee").on("click", function(){
    $(".new_emp").append("<div class='well form-group'>" +
            "<label class='col-sm-4'>Ф.И.О.</label>" +
            "<div class='col-sm-8'>" +
                "<input name='Person[name]' type='text' class='form-control' required>" +
            "</div>" +
            "<label class='col-sm-4'>Занимаемая должность</label>" +
            "<div class='col-sm-8'>" +
                "<input name='Person[position]' type='text' class='form-control' required>" +
            "</div>" +
            "<label class='col-sm-4'>Телефон</label>" +
            "<div class='col-sm-8'>" +
                "<input name='Person[phone]' type='text' class='form-control' required>" +
            "</div>" +
            "<label class='col-sm-4'>Email</label>" +
            "<div class='col-sm-8'>" +
                "<input name='Person[email]' type='email' class='form-control' required>" +
            "</div>" +
        "</div>");
});