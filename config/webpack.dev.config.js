const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const PROJECT_FOLDER_NAME = 'empty_project';

module.exports = {
    entry: './frontAssets/index.js',
    output: {
        path: path.resolve(__dirname, 'web/build/'),
        filename: 'index.js',
        publicPath: `/${PROJECT_FOLDER_NAME}/web/build/`
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /(node_modules|bower_components)/,
                use: [{
                    loader: 'babel-loader'
                }]
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                                sourceMap: true,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                config: {
                                    path: 'postcss.config.js',
                                },
                            },
                        },
                    ],
                }),
            }
        ]
    },
    devServer: {
        disableHostCheck: true,
        port: 5000,
        proxy: {
            '**': {
                target: 'http://localhost:81/'
            }
        }
    },
    plugins: [
        new ExtractTextPlugin('styles.css'),
    ],
    resolve: {
        extensions: [
            '.css',
            '.js',
            '.jsx',
            '.scss',
        ],
    },
}