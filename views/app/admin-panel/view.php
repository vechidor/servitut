<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="application-view">

    <div class="row">
        <div class = "col-md-2 status<?= $model->app_status ?>"><?= $model->status->status_org ?></div>
         <?php if($model->app_status == 4 ): ?>
         <div class="col-md-8 form_letter">
                <?= Html::a('Сформировать письмо', [$url= 'app/admin-panel/print-org-report', 'id' =>$_GET['id']], ['class' => 'btn btn-info']) ?>          
        </div>
        <?php endif ?>
        <?php if($model->comment && $model->app_status == 3 ): ?>
         <div class="col-md-8 blk_info">
                <?= $model->comment ?>         
        </div>
        <?php endif ?>
        <?php if($model->app_status == 2 ): ?>
         <div class="col-md-8 form_letter">
                
            <?php
                Modal::begin([
                'header' => '<div class = "modal_header">Оставить комментарий</div>',
                'toggleButton' => [
                'label' => 'Оставить комментарий',
                'tag' => 'button',
                'class' => 'btn btn-warning',
                 ],
                'footer' => '',
                ]);
             ?>   
            <?php $form = ActiveForm::begin(['action' => ['app/admin-panel/comment', 'id' => $_GET['id']]]); ?>
                <div class="modal-body">
                    <div class="">   
                        <?= $form->field($model, 'comment')->textarea(['rows' => '5']) ?>
                    </div>  
                    <div class="">             
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'comment']) ?>
                    </div> 
                </div>      
            <?php ActiveForm::end(); ?> 
            <?php Modal::end(); ?>    
            
        </div>
        <?php endif ?> 
    </div>
    
    

    <div class="row">
    <div class="col-md-9 row_block">
    <div class="col-md-12  block_btn">   
   
          
    <div class="col-md-4">       
    <?php $form = ActiveForm::begin(['action' => ['app/admin-panel/change-status', 'id' => $_GET['id']]]); ?>
         <div class="col-md-12 slt">
            <?= $form->field($model, 'app_status')->dropDownList($items_status,["prompt"=>"Выберите статус"])->label(''); ?>
        </div>       
    <?php ActiveForm::end(); ?>  
    </div> 
    <div class="col-md-8 knp"> 
        <?php if($model->registr_number): ?>
         <div class="reg_num"> 
             <?= "№ ".$model->registr_number ?>
         </div>     
        <?php endif ?>
     <?php
    Modal::begin([
        'header' => '<div class = "modal_header">Регистрация заявки</div>',
        'toggleButton' => [
        'label' => ($model->registr_number) ? 'Изменить рег. номер' : 'Зарегистрировать заявку',
        'tag' => 'button',
        'class' => ($model->registr_number) ? 'btn btn-link' : 'btn btn-success',
         ],
        'footer' => '',
    ]);
    ?>   
    <?php $form = ActiveForm::begin(['action' => ['app/admin-panel/registration-number', 'id' => $_GET['id']]]); ?>
         <div class="modal-body">
            <div class="">   
            <?= $form->field($model, 'registr_number')->textInput(['placeholder' => 'Внесите данные'],['maxlength' => true])->label('') ?>
            <?= $form->field($model, 'registr_date')->input("date",['placeholder' => 'Дата документа'])->label('') ?> 
            </div>  
            <div class="">             
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div> 
         </div>      
    <?php ActiveForm::end(); ?> 
    <?php Modal::end(); ?>    
    </div>          
    </div>
    </div>     
    </div>       
    
    <?//= DetailView::widget([
//        'model' => $model,
//        'attributes' => [
//            'id',
//            'land_category',
//            'permitted_use',
//            'reg_number',
//            'reg_date',
//            'extract_registry_number',
//            'extract_registry_date',
//            'target_servitut',
//            'title_org_servitut',
//            'term_date_with',
//            'term_date_by',
//            'area_servitut',
//            'scheme_servitut',
//            'report_date',
//            'pay_servitut',
//            'pay_period',
//            'date_preparation',
//            'amount_damages',
//            'loss_profits',
//            'reclamation_project',
//            'project_protocol',
//            'project_doc',
//            'reference_objects',
//            'reference_land',
//            'id_app_org',
//        ],
//    ]) ?>
    
      
<div class="row">
  <div class="col col-md-8 content_application">        
        <div class = "title_application">ЗАЯВКА НА УСТАНОВЛЕНИЕ СЕРВИТУТА</div>
  <div>
  <div class="form-group">
    <label for="">1. Наименование организации</label>
    <div><?= $model->org->fullname; ?></div>
  </div>
  <div class="col-md-12">
    <label for="">2. Сведения о земельном участке, на котором планируется установление 
сервитута:
    </label>  
  </div>
    <div class="col-md-12">     
      <label for=""><?//= $list_land1; ?></label>
    </div>
   <div class="form-group row">
    <div class="col-md-6">
      <label for="">- кадастровый номер;</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model->cadaster_number; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- адрес, место расположение;</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model->land_address; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- площадь (кв.м, га);</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model->land_area; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- категория земель;</label>     
    </div>
    <div class="col-md-6">  
        <?= $model->land->name_land; ?>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- разрешенное использование;</label>     
    </div>
    <div class="col-md-6"> 
        <?= $model->permitted_use; ?>       
    </div>
  </div>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- регистрационная запись государственной регистрации право собственности 
Российской Федерации;</label>
      <div class="col-md-6">
        <div class="form-check txt">
             <?= "№ ".$model->reg_number; ?>                
        </div>
        <div class="form-check txt">
            <?= $model->reg_date; ?> 
        </div>      
      </div>
    </div>
  </fieldset>
   <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- вещное право (постоянное (бессрочное), аренда);</label>
      <div class="col-md-6">
        <div class="form-check txt">
             <?= "№ ".$model->proprietary_reg_number; ?>                
        </div>
        <div class="form-check txt">
            <?= $model->proprietary_reg_date; ?> 
        </div> 
        <div class="form-check txt">
            <?= $model->proprietary_law; ?> 
        </div>  
      </div>
    </div>
  </fieldset>      
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- выписка из единого государственного реестра недвижимости.</label>
      <div class="col-md-6">
        <div class="form-check txt">
            <?= "№ ".$model->extract_registry_number; ?> 
        </div>
        <div class="form-check txt">
            <?= $model->extract_registry_date; ?> 
        </div>      
      </div>
    </div>
  </fieldset>
  <div class="form-group row">  
     <div class="col-md-6">
        <label for="">3. Цель установления сервитута:</label>      
     </div> 
     <div class="col-md-6"> 
       <?= (!empty($model->other_data_servitut)) ? $model->other_data_servitut : $model->target->name_target; ?> 
     </div>
  </div>     
  <div class="form-group row">  
    <div class="col-md-6">
        <label for="">4. Наименовании организации, в интересах которой устанавливается 
        сервитут:
        </label>      
    </div>  
    <div class="col-md-6">
       <?= $model->title_org_servitut; ?>
    </div>
  </div>     
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">5. Срок установления сервитута.</label>
      <div class="col-md-6">
        <div class="form-check txt">
            <?= $model->term_date_with; ?>
        </div>
        <div class="form-check txt">
            <?= $model->term_date_by; ?>
        </div>      
      </div>
    </div>
  </fieldset> 
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">6. Площадь сервитута (кв.м, га).</label>     
    </div>
    <div class="col-md-6"> 
         <?= $model->area_servitut; ?>
    </div>
  </div> 
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">7. Схема границ сервитута на кадастровом плане территории </label>
      <div class="col-md-6"> 
            <?= ($model->scheme_servitut == 1) ? 'представлена' : 'не представлена' ; ?>      
      </div>
    </div>
  </fieldset>
    
  <div class="row col-md-12">
    <label for="">8. Плата за сервитут:</label>     
  </div>     
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">Отчет независимого оценщика:</label>
      <div class="col-md-6">
          <div class="form-check txt">
              <?= $model->org_appraiser; ?>
         </div>
          <div class="form-check txt">
              <?= "№ ".$model->appraiser_registry_number; ?>
         </div>  
         <div class="form-check txt">
              <?= $model->report_date; ?>
         </div>
        <div class="form-check txt">
            <?= $model->pay_servitut." руб."; ?>
        </div>
        <div class="form-check txt">
            <?= $model->pay_period; ?>
        </div>   
      </div>
    </div>
  </fieldset>
  <div class="row col-md-12">
    <label for="">9. Возмещение потерь и убытков:</label>     
  </div> 
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">отчет независимого оценщика:</label>
      <div class="col-md-6">
         <div class="form-check txt">
              <?= $model->date_preparation; ?>
         </div>
        <div class="form-check txt"> 
             <div class="col-md-8 blk_preparation">
                 <?= $model->amount_damages." руб."; ?>
             </div>
             <div class="col-md-4 blk_preparation">
                 <?= $model->damages_nds; ?>
             </div>    
        </div>
         <div class="form-check txt">
             <div class="col-md-8 blk_preparation">
                 <?= $model->loss_profits." руб."; ?>
             </div>
             <div class="col-md-4 blk_preparation">
                 <?= $model->loss_nds; ?>
             </div>
        </div>      
      </div>
    </div>
  </fieldset> 
   <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-sm-6 pt-0">10. Проект рекультивации земель</label>
      <div class="col-md-6">
            <?= ($model->reclamation_project == 1) ? 'представлен' : 'не представлен' ; ?>      
      </div>
    </div>
  </fieldset> 
   <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">11. Проект соглашения об установлении сервитута</label>
      <div class="col-md-6">
             <?= ($model->project_protocol == 1) ? 'представлен' : 'не представлен' ; ?>  
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">12. Проектная документация </label>
      <div class="col-md-6">
             <?= ($model->project_doc == 1) ? 'представлена' : 'не представлена' ; ?>
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">13. Информационная справка об объектах капитального строительства, 
        расположенных на земельном участке
      </label>
      <div class="col-md-6">
            <?= ($model->reference_objects == 1) ? 'представлена' : 'не представлена' ; ?> 
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">14. Информационная справка об использовании земельного участка</label>     
      <div class="col-md-6">  
            <?= ($model->reference_land == 1) ? 'представлена' : 'не представлена' ; ?>  
      </div>
    </div>
  </fieldset>       
   <div class="form-group row">  
  </div>  
</div>        
</div>    
</div> 
<?php if($count_file == true): ?>
 <div class='row col-md-8 info_block_upl'><h3>Все файлы к заявке загружены</h3></div> 
<?php else: ?>
 <div class='row col-md-8 info_block_upl no'><h3>Необходимо загрузить файлы</h3></div> 
<?php endif ?> 
    <div class="row file_upload col-md-8">
   <?php  if(Yii::$app->user->can('upload_file')):   ?> 
        <div class="col-md-8">
            <?= Html::a('Загрузка файлов', $url= 'index.php?r=app/admin-panel/upload-file&id_doc='.$_GET['id'], ['class' => 'btn btn-primary']) ?>      
        </div> 
    <?php endif ?>    
        <?= "<div class='form-row col-md-12'>";  ?>
        <?= "<div class='form-row file_upload_title col-md-12'><h3>Список загруженных файлов.</h3></div>";  ?>
      
     <?php $i = 1; ?>
        <?php foreach ($name_doc_file AS $ndf){ ?> 
          <?= "<div class='col-md-1 fl'>".$i."</div>" ?>   
          <?= "<div class='col-md-11 fl'>". Html::a( $ndf->doc->name_doc, $url= $ndf->path). "</div>"  ?>          
         <?php $i++; ?>
         <?php  } ?>
        <?= "</div>";  ?>
        <div class="col-md-8 btn_zip">
            <?= Html::a('Скачать архив',[ $url= 'app/admin-panel/zip-download', 'id_org' =>$model->id_app_org, 'id_doc' =>$_GET['id']], ['class' => 'btn btn-primary btn-sm']) ?> 
        </div> 
    </div>       
</div>
<?php
$script = <<< JS
        
   $(document).on('change', '#application-app_status', function(){
         $(this).closest('form').submit();
   })
   
JS;
$this->registerJs($script);
?>