<?php
$this->title = "Печать";
?>
<!--<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns:mv="http://macVmlSchemaUri" xmlns="http://www.w3.org/TR/REC-html40">-->

<head>
    <meta name=Название content=""/>
    <meta name="Ключевые слова" content=""/>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <meta name=ProgId content=Word.Document/>
    <meta name=Generator content="Microsoft Word 14"/>
    <meta name=Originator content="Microsoft Word 14"/>
   
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
        </w:WordDocument>
    </xml>

    <style>
        .content_blank_agenda{
            text-indent: 2.5em;
             line-height: 1.5em;
             text-align: justify;
             font-size: 19px;
        } 
    </style>
</head>
<body>
    
 <?php 

    $i = 1;
        foreach($options_agenda as $option){ 
            
             $date_by = date_create($option->term_date_by);
             $date_with = date_create($option->term_date_with);
             $interval = date_diff($date_by, $date_with);
    
             $date_interval = $interval->format('%m мес');     
            
 ?>
  <table class="content_blank_agenda" style="width: 100%;"> 
      <tr>
          <td>
        <div>ID заявки <?=$option->id?>  Исходящий № <?=$option->customer_registr_number?>  Входящий № <?=$option->registr_number?></div> 
        <br>
        <div><?= $i?>. Обращение <strong><span style='background:yellow;mso-highlight:yellow;'><?=$option->org->fullname?></span></strong> (далее – <?=$option->org->short_name?>) от <?=$option->customer_registr_date?> 
            № <?=$option->customer_registr_number?> по вопросу согласования установления сервитута на земельном участке с 
            кадастровым номером <?=$option->cadaster_number?>.</div>
        <div>Земельный участок с кадастровым номером <?=$option->cadaster_number?>, общая площадь 
            <?=$option->land_area?> кв. м, расположен по адресу: <?=$option->land_address?>, категория земель: <?=$option->land->name_land?>,
            разрешенное использование: <?=$option->permitted_use?>.</div>
        <div>Право собственности Российской Федерации на земельный участок(регистрационная запись в ЕГРН от <?=$option->reg_date?> № <?=$option->reg_number?>)и  право постоянного (бессрочного) пользования <?=$option->org->short_name?> (регистрационная запись в ЕГРН 
            от <?=$option->proprietary_reg_date?> № <?=$option->proprietary_reg_number?>) зарегистрированы в установленном законом порядке.</div>
        <div>Сервитут устанавливается в интересах <?=$option->title_org_servitut?>.</div>
        <div>Цель установления сервитута - <?= (!empty($option->other_data_servitut)) ? $option->other_data_servitut : $option->target->name_target; ?>.</div>
        <div>Площадь установления сервитута составляет <?=$option->area_servitut?> кв. м.</div> 
        <div>Срок установления сервитута – <?=$date_interval?>.</div>
        <div>Плата за установление сервитута на земельном участке с кадастровым номером <?=$option->cadaster_number?> установлена
            на основании  Отчета об оценке № <?=$option->appraiser_registry_number?> от <?=$option->report_date?>, подготовленный <?=$option->org_appraiser?>, в 
            размере <?=$option->pay_servitut?> рублей за <?=$option->pay_period?>.</div> 
        <div>Проект соглашения об установлении сервитута <?=($option->project_protocol == 1) ? 'представлен' : 'не представлен'?>.</div>  
        <div>В справке от <?=$option->customer_registr_date?>  <?=$option->org->short_name?> сообщает, что земельный участок площадью <?=$option->area_servitut?> кв. м 
             планируемый под сервитут <span style='background:yellow;mso-highlight:yellow;'>свободен от объектов недвижимого имущества</span>, закрепленного за <?=$option->org->short_name?>,
             и не используется учреждением. В связи с этим <?=$option->org->short_name?> <span style='background:yellow;mso-highlight:yellow;'>не несет убытки</span>.</div>
        <div>Проектная документация <?=($option->project_doc == 1) ? 'представлена' : 'не представлена'?>.</div>
        <div>Схемы границ сервитута на кадастровом плане территории <?=($option->scheme_servitut == 1) ? 'представлена' : 'не представлена'?>.</div>
        <div>Информационная справка об объектах капитального строительства, расположенных на земельном участке с кадастровым номером <?=$option->cadaster_number?>, <?=($option->reference_objects == 1) ? 'представлена' : 'не представлена'?>.</div>

        <div>На решение Комиссии</div>
            <br>
            <br>
            <br>
        </td>
    </tr>
  </table>
 <?php   
       $i++;  
    }
 ?>     
</body>
</html>