<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title= "Загрузка файла";
$this->params['breadcrumbs'][] = ['label'=>'Заявка', 'url'=>['app/admin-panel/view' , 'id' => $id_doc]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row col-md-8 content_upl_file">   
<h4>Загрузка файла</h4>
<?php $form = ActiveForm::begin([
    'id' => 'doc-file',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-4\"></div><div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-4 control-label']
    ]
]); ?>

<?= "<div class='form-row col-md-12'>";  ?>      
    <?php foreach ($name_doc_file AS $ndf){ ?>   
          <?= "<div class='col-md-10 fl'>". Html::a( $ndf->doc->name_doc, $url= $ndf->path). "</div>"  ?> 
          <?= "<div class='col-md-2 fl'>". Html::a('Удалить', ['filedelete', 'id' => $ndf->id, 'id_doc' => $id_doc, 'path_file' => $ndf->path], ['class' => 'btn btn-primary'])."</div>"  ?>       
    <?php  } ?> 
<?= "</div>";  ?>

<?php if($count_file == true): ?>
    <div class="form-row col-md-12">        
            <div class='col-md-12 info_block_upl'><h3>Все файлы к заявке загружены</h3></div>           
<?php else: ?>
        <div class="form-row col-md-12 block_upl_file">
<?= $form->field($file, 'title')->dropDownList($name_doc,["prompt"=>"Выберите необходимый документ"])->label('Документ'); ?>
<?= $form->field($file, 'id_doc')->textInput(["value"=>$id_doc, "type"=>"hidden"]); ?>
              
    <?php if(!empty($id_doc)): ?>
        <?= $form->field($file_upload, 'fileDoc')->fileInput(['class' =>'btn btn-primary'])?>
            <div class="">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
    <?php else: ?>
            <div class='col-md-12'><h2>Заявка не создана</h2></div>       
    <?php endif ?> 
        </div>
<?php endif ?>        
    </div>               
<?php ActiveForm::end(); ?>
</div> 
