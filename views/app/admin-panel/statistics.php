<?php
use app\assets\HighchartsAsset;
use miloschuman\highcharts\Highcharts;
//use highcharts\Highcharts;



//HighchartsAsset::register($this);
//$this->title = 'Highcharts Test';


?>

<?php         
          echo Highcharts::widget([
   'options' => [
      'title' => ['text' => 'Fruit Consumption'],
      'xAxis' => [
         'categories' => ['Apples', 'Bananas', 'Oranges']
      ],
      'yAxis' => [
         'title' => ['text' => 'Fruit eaten']
      ],
      'series' => [
         ['name' => 'Jane', 'data' => [1, 0, 4]],
          
         ['name' => 'John', 'data' => [5, 7, 3]]
      ]
   ]
]);   
          
  