<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'land_category') ?>

    <?= $form->field($model, 'permitted_use') ?>

    <?= $form->field($model, 'reg_number') ?>

    <?= $form->field($model, 'reg_date') ?>

    <?php // echo $form->field($model, 'extract_registry_number') ?>

    <?php // echo $form->field($model, 'extract_registry_date') ?>

    <?php // echo $form->field($model, 'target_servitut') ?>

    <?php // echo $form->field($model, 'title_org_servitut') ?>

    <?php // echo $form->field($model, 'term_date_with') ?>

    <?php // echo $form->field($model, 'term_date_by') ?>

    <?php // echo $form->field($model, 'area_servitut') ?>

    <?php // echo $form->field($model, 'scheme_servitut') ?>

    <?php // echo $form->field($model, 'report_date') ?>

    <?php // echo $form->field($model, 'pay_servitut') ?>

    <?php // echo $form->field($model, 'pay_period') ?>

    <?php // echo $form->field($model, 'date_preparation') ?>

    <?php // echo $form->field($model, 'amount_damages') ?>

    <?php // echo $form->field($model, 'loss_profits') ?>

    <?php // echo $form->field($model, 'reclamation_project') ?>

    <?php // echo $form->field($model, 'project_protocol') ?>

    <?php // echo $form->field($model, 'project_doc') ?>

    <?php // echo $form->field($model, 'reference_objects') ?>

    <?php // echo $form->field($model, 'reference_land') ?>

    <?php // echo $form->field($model, 'id_app_org') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
