<?php
$this->title = "Печать";
?>
<!--<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns:mv="http://macVmlSchemaUri" xmlns="http://www.w3.org/TR/REC-html40">-->

<head>
    <meta name=Название content=""/>
    <meta name="Ключевые слова" content=""/>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8"/>
    <meta name=ProgId content=Word.Document/>
    <meta name=Generator content="Microsoft Word 14"/>
    <meta name=Originator content="Microsoft Word 14"/>
   
    <xml>
        <w:WordDocument>
            <w:View>Print</w:View>
        </w:WordDocument>
    </xml>

    <style>
        .header_blank .header_blank{
           width: 100%;
        }      
        .header_blank .header_blank td {
             border: none;
             text-align: justify;
             font-size: 18px;
        }
        .content_blank div{
             text-indent: 2.5em;
             line-height: 1.5em;
             text-align: justify;
             font-size: 19px;
        }
        .content_blank p{
            text-align: justify;
        }
        .name_org {
            font-size: 19px;
            width: 50%;
            text-align:justify;
            border: none;
        }
        .con_agre {
            width: 80%;
            font-size: 12px;
        }

    </style>
</head>
<body>
<?php 

    $date_by = date_create($options->term_date_by);
    $date_with = date_create($options->term_date_with);
    $interval = date_diff($date_by, $date_with);
       
    $date_interval = $interval->format('%m мес'); 
?>
    
     <table class="header_blank">
        <tr>           
            <td style="width: 60%; border: none;"></td>
            <td class = "name_org">
                <p><?=$options->org->fullname?></p>                
                <p style="text-align: left;"><?=$options->org->addresses?></p>               
            </td>            
        </tr>
        <br>
        <tr>
            <td style="border: none;">
                <table class="con_agre">
                  <tr>
                    <td style="border: none;">
                      <p>
                         О даче согласия на заключение
                         соглашения об установлении сервитута
                     </p>
                    </td>
                  </tr>
               </table>  
            </td>
            <td style="border: none;"></td>            
        </tr>
    </table>
    <br>
    <table class="content_blank" style="width: 100%;">
       <tr>
         <td>
           <div>Министерство науки и высшего образования Российской Федерации рассмотрело обращения <?=$options->org->fullname?> 
               (далее – <?=$options->org->short_name?>) от <?=$options->customer_registr_date?> № <?=$options->customer_registr_number?> по вопросу 
               согласования установления сервитута на земельном участке с кадастровым номером <?=$options->cadaster_number?> и 
               сообщает следующее.</div>
              <div> В соответствии с Указом Президента Российской Федерации
               от 15.05.2018 № 215 «О структуре федеральных органов исполнительной власти», постановлением Правительства
               Российской Федерации от 15.06.2018 № 682 «Об утверждении Положения о Министерстве науки и высшего образования
               Российской Федерации и признании утратившими силу некоторых актов Правительства Российской Федерации», 
               распоряжением Правительства Российской Федерации от 27.06.2018 № 1293-р «Об утверждении перечней организаций,
               подведомственных Министерству науки и высшего образования Российской Федерации, Министерству просвещения 
               Российской Федерации, Рособрнадзору и признании утратившими силу актов Правительства Российской Федерации»,
               ч. 2 ст. 39.24 Земельного кодекса Российской Федерации, решением Комиссии по рассмотрению вопросов управления, 
               списания, распоряжения федеральным имуществом, закрепленным за федеральными государственными учреждениями,
               подведомственными Министерству науки и высшего образования Российской Федерации, и совершения ими крупных 
               и иных сделок (протокол от 03.06.2019 № 12/У-19) Минобрнауки России согласовывает установление сервитута 
               площадью <?=$options->area_servitut?> кв. м на земельном участке с кадастровым номером <?=$options->cadaster_number?>.</div>
               <div>Земельный участок с кадастровым номером <?=$options->cadaster_number?>, общая площадь <?=$options->land_area?> кв. м, 
               расположен по адресу: <?=$options->land_address?>, 
               категория земель: <?=$options->land->name_land?>, разрешенное использование: <?=$options->permitted_use?>.</div>
               <div>Сервитут устанавливается в интересах <?=$options->title_org_servitut?>.</div>
               <div>Цель установления сервитута -  <?= (!empty($options->other_data_servitut)) ? $options->other_data_servitut : $options->target->name_target; ?> .</div>
               <div>Площадь установления сервитута составляет <?=$options->area_servitut?> кв. м.</div>
               <div>Срок установления сервитута – <?=$date_interval?>.</div>
               <div>Плата за установление сервитута на земельном участке с кадастровым номером <?=$options->cadaster_number?> установлена 
               на основании  Отчета об оценке № <?=$options->appraiser_registry_number?> от <?=$options->report_date?>, подготовленного <?=$options->org_appraiser?>,  в  
               размере <?=$options->pay_servitut?> рублей за <?=$options->pay_period?>.</div> 
               <div>В справке от <?=$options->customer_registr_date?>  <?=$options->org->short_name?> сообщает, что земельный участок площадью <?=$options->area_servitut?> кв. м, 
               планируемый под сервитут, <span style='background:yellow;mso-highlight:yellow;'>свободен от объектов недвижимого имущества и не используется <?=$options->org->short_name?>,
               в связи с чем <?=$options->org->short_name?> не понесет убытки от установления сервитута на указанном земельном участке.</span></div>  
               <div>В течение 3 дней после заключения соглашения об установлении сервитута на земельном участке с кадастровым 
               номером <?=$options->cadaster_number?> <?=$options->org->short_name?> необходимо направить копию подписанного соглашения в адрес Минобрнауки России.
                </div>
         </td>
       </tr>
   </table>          
</body>
</html>