<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Collapse;
use yii\widgets\ActiveField;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="row">
        <div class="col-md-10 title_adn">
            <label><h3>Заявки организаций</h3></label>
        </div>
    </div>
    <div class="container">
      <div class="col-md-12 blk_hd"> 
        <div class="btn_filter col-md-2">Фильтр</div>
    
          <div class="col-md-10 send_agenda">
            
 <?php      Modal::begin([
                'header' => '<div class = "modal_header">Вывести на повестку дня</div>',
                'toggleButton' => [
                'label' => 'Вывести на повестку',
                'tag' => 'button',
                'class' => 'btn btn-info',
                ],
                 'footer' => '',
                ]);
     
                 $form = ActiveForm::begin(['action' => ['app/admin-panel/print-put-agenda']]); ?>
       
          <div class="modal-body-agenda">

 <?php   foreach ($requests_agenda as $req_ag){
     
         echo  "<div class='col-md-12 blk_content'>"  
                 . "<div class='col-md-3 chbox'>"
                     . $form->field($filter, 'checkbox_app_org[]')->checkbox(['value' =>$req_ag->id,
                         'class'=>'application-checkbox_app_org',         
                         'label' => 'Вкл/Откл',
                          'labelOptions' => [
                          'style' => 'padding-left:20px;',
                           'for' => '',   
                         ],
                           'disabled' => false
                         ])
                  ."</div>"     
                     ."<div class='col-md-9 indf_org'>"
                        . "<span>ID " .$req_ag->id . "</span><span>Исх. № " .$req_ag->customer_registr_number . "</span><span>Вх. № " .$req_ag->registr_number ."</span>" 
                     ."</div>"
                     ."<div class='col-md-9 title_org_agenda'>"                        
                        . $req_ag->org->fullname            
                     ."</div>"     
               ."</div>";
      
         } ?>
                 
            <div class="row"> 
                 <div class="col-md-5 blk_all"> 
                    <?= Html::tag('a','Выбрать все /',['class' => 'checkbox_all']) ?>
                    <?= Html::tag('a','Убрать все',['class' => 'checkbox_no_all']) ?>              
                 </div> 
            </div>   
            <div class="row"> 
                <div class="col-md-12 blk_btn_agenda">  
                    <div class="col-md-6"> 
                    <?= Html::submitButton('Сформировать повестку', ['class' => 'btn btn-primary btn_agenda']) ?>                      
                    </div>
                    <div class="col-md-6"> 
                        <?= Html::tag('div','Закрыть окно',['class' => 'btn btn-primary' , 'data-dismiss' => "modal"]) ?>              
                    </div>
                </div> 
            </div> 
          </div>      
    <?php ActiveForm::end(); ?> 
    <?php Modal::end(); ?>                            
        </div>
      
<div class="admin_filter col-md-12" style="display: none">
  <?php  $form = ActiveForm::begin([
        'layout'=>'horizontal',
        'options' => ['class' => 'form-status']]); 
   
 echo "<div class='form-row col-md-10'>"
        . "<div class='col-md-6'>"
            . $form->field($filter, 'search_status')->dropDownList($items_status,["prompt"=>"Выберите статус"])->label('Поиск по статусу',['class'=>'control-label col-sm-6'])
        . "</div>"
        . "<div class='col-md-6'>"
            . $form->field($filter, 'checkbox_status')->checkbox([
             'label' => 'Вкл/Откл поиск по статусу',
             'labelOptions' => [
             'style' => 'padding-left:20px;'
             ],
             'disabled' => false
            ])
         ."</div>"        
    ."</div>" 
    ."<div class='form-row col-md-10'>"
        . "<div class='col-md-6'>"
            . $form->field($filter, 'search_servitut')->dropDownList($items_target,["prompt"=>"Выберите статус"])->label('Поиск по цели установления сервитута',['class'=>'control-label col-sm-6'])
        . "</div>"
        . "<div class='col-md-6'>"
            . $form->field($filter, 'checkbox_servitut')->checkbox([
             'label' => 'Вкл/Откл поиск по цели установления сервитута',
             'labelOptions' => [
             'style' => 'padding-left:20px;'
             ],
             'disabled' => false
            ])
         ."</div>"        
    ."</div>"  
    
         ."<div class='form-row col-md-10 title_search_org'>Поиск по организации</div>"
         
         ."<div class='form-row col-md-12 blk_search_org'>"   
        
         . "<div class='col-md-12'>";

        foreach ($all_organization as $all_org){
     
         echo  "<div class='col-md-12 blk_org_content'>"  
                 . "<div class='col-md-1 chbox'>"
                     . $form->field($filter, 'checkbox_org[]')->checkbox(['value' =>$all_org->id,
                         'class'=>'application-checkbox_org',         
                         'label' => '',
                          'labelOptions' => [
                          'style' => 'padding-left:20px;',
                           'for' => '',   
                         ],
                           'disabled' => false
                         ])
                  ."</div>"     
                  ."<div class='col-md-11 title_org'>"     
                    . $all_org->fullname            
                  ."</div>"     
               ."</div>";     
         } 
 
  echo  "</div>"       
         . "</div>" 
         . "<div class='form-row col-md-10'>"
         ."<div class='col-md-3'>"            
         ."</div>"      
         ."<div class='col-md-2'>"
             .Html::submitButton('Поиск', ['class' => 'btn btn-primary', 'name' => 'login-button']) 
         ."</div>" 
         ."<div class='col-md-2'>"           
           . Html::a('Очистить', ['clear'], ['class' => 'btn btn-primary'])
         ."</div>"
         . "</div>"
    ."</div>" ;     
    ActiveForm::end(); ?>  
 </div>  
</div>
</div>         
 
    <div class="container">
    <div class="row app_pnl">
        <div class="col-md-1"><div class="col-md-6">№</div>          
        <div class="col-md-6 ttl">ID</div></div>      
        <div class="col-md-5">Наименование организации</div>       
        <div class="col-md-2">Статус</div> 
        <div class="col-md-2">Исх. № и дата</div>
        <div class="col-md-2">№ и дата входящего документа Минобрнауки России</div> 
    </div>
    <?php $n = 1; ?>   
    <?php foreach ($all_app_org AS $al_org){        
          echo Html::a("<div class = 'row app_one'>"
               . "<div class='col-md-1'><div class='col-md-6 ttl'>".$n."</div>"
               . "<div class='col-md-6 ttl'>".$al_org->id."</div></div>"              
               . "<div class='col-md-5 ttl'><div><div>".$al_org->org->fullname."</div><div class = 'col-md-8 send_date'>Дата отправки заявка : ".$al_org->date_send."</div></div></div>"
               . "<div class='col-md-2 ttl inx_status$al_org->app_status'>".$al_org->status->status_org."</div>"
               . "<div class='col-md-2 ttl'><div><div>№ : ".$al_org->customer_registr_number."</div><div>Дата : ".$al_org->customer_registr_date."</div></div></div>"  
               . "<div class='col-md-2 ttl'><div><div>№ : ".$al_org->registr_number."</div><div>Дата : ".$al_org->registr_date."</div></div></div>"  
               . "</div>", ['app/admin-panel/view' , 'id' => $al_org->id]) ;        
      $n++;      
    } ?>
    </div>
<?php // var_dump($al_org->org);  ?>
    <?//= GridView::widget([
       // 'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
      //  'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

          // 'id',
           // 'land_category',
            //'permitted_use',
           // 'reg_number',
           // 'reg_date',
            //'extract_registry_number',
            //'extract_registry_date',
            //'target_servitut',
            //'title_org_servitut',
            //'term_date_with',
            //'term_date_by',
            //'area_servitut',
            //'scheme_servitut',
            //'report_date',
            //'pay_servitut',
            //'pay_period',
            //'date_preparation',
            //'amount_damages',
            //'loss_profits',
            //'reclamation_project',
            //'project_protocol',
            //'project_doc',
            //'reference_objects',
            //'reference_land',
            //'id_app_org',

           // ['class' => 'yii\grid\ActionColumn'],
       // ],
   // ]); ?>
</div>
<?php

$script = <<< JS
           
       $("#application-checkbox_status, #application-checkbox_servitut, #application-checkbox_org, #application-checkbox_app_org").change(function () {
            var checked = $(this).is(':checked');
            if (checked) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
        
        $('.checkbox_all').click(function () {    
              $('.application-checkbox_app_org').prop('checked', true);
        });
        
         $('.checkbox_no_all').click(function () {       
             $('.application-checkbox_app_org').prop('checked', false);    
        });

        
        $('.btn_agenda').click(function () {  
           var elem = $('.application-checkbox_app_org'); 
           var itog = 0;
            for(var j = 0; j < elem.length; j++) {
              if(elem.eq(j).prop("checked")) { itog++; }
            }
             if(itog == 0){
                alert("Документ не сформирован! Выберите необходимые организации.");                             
               return false;
             }   
        });

           
        $(".btn_filter").on("click", function() {
             $(".admin_filter").toggle(200);
        });
        
JS;
$this->registerJs($script);
?>