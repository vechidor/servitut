  <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-form">

    <?php $form = ActiveForm::begin(); ?>
        
<div class="row">
    <div class="col col-md-8 content_application">        
        <div class = "title_application">ЗАЯВКА НА УСТАНОВЛЕНИЕ СЕРВИТУТА</div>

  <div class="form-group">
    <label for="">1. Наименование организации</label>
    <div><?= $name_org; ?></div>
  </div>
  <div class="col-md-12">
    <label for="">2. Сведения о земельном участке, на котором планируется установление 
сервитута:
    </label>
      <?= $form->field($model, 'id_land')->dropDownList($list_land,['prompt'=>'Выберите земельный участок'])->label(''); ?> 
  </div>
   <div class="form-row">
    <div class="col-md-6">
      <label for="">- кадастровый номер;</label>     
    </div>
    <div class="col-md-6">     
      <label class = "cadaster_number"><?=  ($model->cadaster_number) ?? "____________________________________________"; ?></label>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6 ">
      <label for="">- адрес, место расположение;</label>     
    </div>
    <div class="col-md-6">     
      <label class = "land_address"><?= ($model->land_address) ?? "____________________________________________"; ?></label>
    </div>
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <label for="">- площадь (кв.м, га);</label>     
    </div>
    <div class="col-md-6">     
      <label class = "land_area"><?= ($model->land_area) ?? "____________________________________________"; ?></label>     
    </div>     
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <label for="">- категория земель;</label>     
    </div>
    <div class="col-md-6">     
      <label class = "category_land"><?= ($model->land->name_land) ?? "____________________________________________"; ?></label>     
    </div>  
  </div>
  <div class="form-row">
    <div class="col-md-6">
      <label for="">- разрешенное использование;</label>     
    </div>
    <div class="col-md-6">     
       <?= $form->field($model, 'permitted_use')->textInput(['placeholder' => 'Внесите данные'],['maxlength' => true])->label('') ?>
    </div>
  </div>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <div class="col-md-6">
      <label for="">- регистрационная запись государственной регистрации право собственности 
Российской Федерации;</label>
     </div>
       <div class="form-group col-md-6">
        <div class="form-check txt">
            <?= $form->field($model, 'reg_number')->textInput(['placeholder' => 'Номер документа'])->label('') ?>    
        </div>
        <div class="form-check txt">
            <?= $form->field($model, 'reg_date')->input("date",['placeholder' => 'Дата документа'])->label('') ?>           
        </div>      
      </div>
    </div>
  </fieldset>
   <fieldset class="form-group col-md-12">
    <div class="row">
      <div class="col-md-6">
      <label for="">- вещное право (постоянное (бессрочное), аренда);</label>
     </div>
       <div class="form-group col-md-6">
        <div class="form-check txt">
            <?= $form->field($model, 'proprietary_reg_number')->textInput(['placeholder' => 'Номер документа'])->label('') ?>    
        </div>
        <div class="form-check txt">
            <?= $form->field($model, 'proprietary_reg_date')->input("date",['placeholder' => 'Дата документа'])->label('') ?>           
        </div>
        <div class="form-check txt">                
         <?php $params = ['prompt' => 'Выберите статус...']; ?>      
         <?= $form->field($model, 'proprietary_law')->dropDownList([
             'постоянное (бессрочное)' => 'постоянное (бессрочное)',
             'аренда' => 'аренда'            
         ], $params)->label(''); ?> 
        </div>      
      </div>
    </div>
  </fieldset>      
  <fieldset class="form-group col-md-12">
    <div class="form-row">
        <div class="form-group col-md-6">
         <label for="">- выписка из единого государственного реестра недвижимости.</label>
       </div>
      <div class="form-group col-md-6">
        <div class="form-check txt">
            <?= $form->field($model, 'extract_registry_number')->textInput(['placeholder' => 'Номер документа'])->label('') ?>          
        </div>
        <div class="form-check txt">
             <?= $form->field($model, 'extract_registry_date')->input("date",['placeholder' => 'Дата документа'])->label('') ?>
        </div>      
      </div>
    </div>
  </fieldset>
  <div class="form-row">      
  <div class="col-md-6">
    <label for="">3. Цель установления сервитута:</label>      
  </div>
  <div class="col-md-6"> 
       <?php 
    $params = [
        'prompt' => 'Выберите статус...',
    ];
    ?>   
       <?= $form->field($model, 'target_servitut')->dropDownList($items_target,$params)->label(''); ?> 
       <div class = "input_data_servitut" style = "display:none"> 
       <?= $form->field($model, 'other_data_servitut')->textInput(['placeholder' => 'Внесите данные'])->label(''); ?> 
       </div>
  </div>
  </div>
  <div class="form-row">        
  <div class="col-md-6">
    <label for="">4. Наименовании организации, в интересах которой устанавливается 
     сервитут:
    </label>      
  </div>  
  <div class="col-md-6">  
       <?= $form->field($model, 'title_org_servitut')->textInput(['placeholder' => 'Внесите данные'])->label('') ?>       
  </div>
  </div>    
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">5. Срок установления сервитута.</label>
      <div class="col-md-6">
        <div class="form-check txt">
        <?= $form->field($model, 'term_date_with')->input("date",['placeholder' => 'Дата с...'])->label('Дата с...') ?>         
        </div>
        <div class="form-check txt">
            <?= $form->field($model, 'term_date_by')->input("date",['placeholder' => 'Дата по...'])->label('Дата по...') ?>         
        </div>      
      </div>
    </div>
  </fieldset> 
  <div class="form-row">
    <div class="col-md-6">
      <label for="">6. Площадь сервитута (кв.м).</label>     
    </div>
    <div class="col-md-6"> 
        <?= $form->field($model, 'area_servitut')->textInput(['placeholder' => 'Числовое поле'])->label('') ?>       
    </div>
  </div> 
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">7. Схема границ сервитута на кадастровом плане территории </label>
      <div class="col-md-6">        
         <?= $form->field($model, 'scheme_servitut')->radioList( [1=>'представлена', 0 => 'не представлена'])->label(''); ?>       
      </div>
    </div>
  </fieldset>
     <div class="form-group col-md-12">
        <label for="">8. Плата за сервитут:</label>     
    </div> 
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="form-group col-md-6">Отчет независимого оценщика:</label>
      <div class="col-md-6">
          <div class="form-check txt"> 
         <?= $form->field($model, 'org_appraiser')->textInput(['placeholder' => 'Внесите данные'])->label('Организация оценщик') ?>          
        </div>
         <div class="form-check txt"> 
         <?= $form->field($model, 'appraiser_registry_number')->textInput(['placeholder' => 'Номер документа'])->label('Регистрационной номер оценщика') ?>          
        </div> 
         <div class="form-check txt"> 
         <?= $form->field($model, 'report_date')->input("date",['placeholder' => 'Дата подготовки'])->label('дата подготовки отчета') ?>          
        </div>
        <div class="form-check txt">
         <?= $form->field($model, 'pay_servitut')->textInput(['placeholder' => 'Внесите данные'])->label('плата за установление сервитута (руб)') ?> 
        </div>
        <div class="form-check txt">                
         <?php $params = ['prompt' => 'Выберите статус...']; ?>      
         <?= $form->field($model, 'pay_period')->dropDownList([
             'месяц' => 'месяц',
             'год' => 'год',
             'разовый платеж за весь период'=>'разовый платеж за весь период'
         ], $params)->label('период внесения платы (месяц, год, разовый платеж за весь период)'); ?> 
        </div>   
      </div>
    </div>
  </fieldset>
  <div class="col-md-12">
    <label for="">9. Возмещение потерь и убытков:</label>     
  </div> 
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="form-group col-md-6">отчет независимого оценщика:</label>
      <div class="col-md-6">
         <div class="form-check txt">
             <?= $form->field($model, 'date_preparation')->input("date",['placeholder' => 'Дата подготовки'])->label('дата подготовки') ?> 
        </div>
        <div class="form-check txt"> 
          <div class="col-md-8 blk_preparation">  
             <?= $form->field($model, 'amount_damages')->textInput(['placeholder' => 'Внесите данные'])->label('размер убытков (руб.)') ?> 
          </div>    
          <div class="col-md-4 blk_preparation">
             <?php $params = ['prompt' => 'Выберите статус...']; ?>      
             <?= $form->field($model, 'damages_nds')->dropDownList([
             'включая НДС' => 'включая НДС',
             'не включая НДС' => 'не включая НДС'             
             ], $params)->label('с НДС / без НДС'); ?> 
          </div>
         </div>
        <div class="form-check txt">
          <div class="col-md-8 blk_preparation">      
             <?= $form->field($model, 'loss_profits')->textInput(['placeholder' => 'Внесите данные'])->label('упущенной выгоды (руб.)') ?> 
          </div>   
          <div class="col-md-4 blk_preparation">
             <?php $params = ['prompt' => 'Выберите статус...']; ?>      
             <?= $form->field($model, 'loss_nds')->dropDownList([
             'включая НДС' => 'включая НДС',
             'не включая НДС' => 'не включая НДС'             
             ], $params)->label('с НДС / без НДС'); ?> 
          </div>
        </div>
    </div>
  </fieldset> 
   <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-md-6">10. Проект рекультивации земель</label>
      <div class="col-md-6">
        <div class="form-check">
          <?= $form->field($model, 'reclamation_project')->radioList( [1=>'представлен', 0 => 'не представлен'])->label(''); ?>
        </div>        
      </div>
    </div>
  </fieldset> 
   <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-md-6">11. Проект соглашения об установлении сервитута</label>
      <div class="col-md-6">
        <div class="form-check">
          <?= $form->field($model, 'project_protocol')->radioList( [1=>'представлен', 0 => 'не представлен'])->label(''); ?>
        </div>    
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-md-6">12. Проектная документация </label>
      <div class="col-md-6">
        <div class="form-check">
            <?= $form->field($model, 'project_doc')->radioList( [1=>'представлена', 0 => 'не представлена'])->label(''); ?>    
        </div>  
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-md-6">13. Информационная справка об объектах капитального строительства, 
        расположенных на земельном участке
      </label>
      <div class="col-md-6">
        <div class="form-check">
            <?= $form->field($model, 'reference_objects')->radioList( [1=>'представлена', 0 => 'не представлена'])->label(''); ?>  
        </div>  
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-md-6">14. Информационная справка об использовании земельного участка</label>     
      <div class="col-md-6">
        <div class="form-check">
            <?= $form->field($model, 'reference_land')->radioList( [1=>'представлена', 0 => 'не представлена'])->label(''); ?>  
        </div>    
      </div>
    </div>
  </fieldset>        
   <div class="form-group row">
    <div class="col-md-12">
       <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
  </div>
 </div>    
</div>  
    <?php ActiveForm::end(); ?>
 
</div>
<?php

$script = <<< JS
        
    $("#application-id_land").change(function(){      
        var id_land_value = document.querySelector('#application-id_land').value;
             url = "index.php?r=app/doc/index"; 				
     
             document.querySelector('.cadaster_number').innerText = '';
             document.querySelector('.land_address').innerText = '';
             document.querySelector('.land_area').innerText = '';
             document.querySelector('.category_land').innerText = '';
  		
		$.ajax({
			url: url,
			type: 'POST',
                        dataType: 'json',       
			data: {id_land : id_land_value,
			},
			success: function(json){
                            document.querySelector('.cadaster_number').append(json[0].kadastr_nomer);
                            document.querySelector('.land_address').append(json[0].address);
                            document.querySelector('.land_area').append(json[0].area);
                            document.querySelector('.category_land').append(json.category_land);                            
					},
                        error: function(json){
                            alert('Ошибка загрузки');
                                       }      
			});				
    });
   
   var target_iteam = document.querySelector('#application-target_servitut'); 
   var input_data_servitut = document.querySelector('.input_data_servitut'); 
   var other_elem = document.querySelector('#application-other_data_servitut');      
        if(target_iteam.value == 5){        
            input_data_servitut.style.display = "block";    
         }
        
      $("#application-target_servitut").change(function(){ 
         if(target_iteam.value == 5){      
             input_data_servitut.style.display = "block";
        }else{
             input_data_servitut.style.display = "none";
        other_elem.value = "";
        }
    });    
JS;
$this->registerJs($script);
?>