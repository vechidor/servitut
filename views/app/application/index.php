<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="container">
    <div class="row col-md-12">
        <label><h3><?= $name_org; ?></h3></label>
    </div>
    </div>    
    <div class="container">
    <div class="row app_pnl">
        <div class="col-md-1">ID</div>          
        <div class="col-md-2">Кадастровый номер<br> земельного участка</div>
        <div class="col-md-4">Адрес земельного участка</div>
        <div class="col-md-2">Статус</div>
        <div class="col-md-2">Исх. № и дата</div>
        <div class="col-md-1">Площадь сервитута</div>        
    </div>      
     <?php  foreach ($all_app AS $al){      
                echo Html::a("<div class = 'row app_one'>"
               . "<div class='col-md-1 ttl'>".$al->id."</div>"
               . "<div class='col-md-2 ttl'>".$al->cadaster_number."</div>"
               . "<div class='col-md-4 ttl'>".$al->land_address."</div>"
               . "<div class='col-md-2 ttl inx_status$al->app_status'>".$al->status->status_org."</div>"       
               . "<div class='col-md-2 ttl'><div><div>№ : ".$al->customer_registr_number."</div><div>Дата : ".$al->customer_registr_date."</div></div></div>"
               . "<div class='col-md-1 ttl'>".$al->land_area."</div>"
               . "</div>", ['app/application/view' , 'id' => $al->id]) ;   
     } ?>
    </div>
    
    <?//= GridView::widget([
        //'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
       // 'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

           //'id',
           // 'land_category',
            //'permitted_use',
           // 'reg_number',
           // 'reg_date',
            //'extract_registry_number',
            //'extract_registry_date',
            //'target_servitut',
            //'title_org_servitut',
            //'term_date_with',
            //'term_date_by',
            //'area_servitut',
            //'scheme_servitut',
            //'report_date',
            //'pay_servitut',
            //'pay_period',
            //'date_preparation',
            //'amount_damages',
            //'loss_profits',
            //'reclamation_project',
            //'project_protocol',
            //'project_doc',
            //'reference_objects',
            //'reference_land',
            //'id_app_org',

      //      ['class' => 'yii\grid\ActionColumn'],
     //   ],
  //  ]); ?>
</div>
