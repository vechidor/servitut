<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = 'Изменить заявку: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Завки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить заявку';
?>
<div class="application-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'name_org' => $name_org,
                    'list_land'=>$list_land, 'items_target' => $items_target,
                   
    ]) ?>

</div>
