<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model_app_view app\model_app_views\Application */

$this->title = $model_app_view->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="application-view">

    <div class="row">
            <div class = "col-md-2 status<?= $model_app_view->app_status ?>"><?= $model_app_view->status->status_org ?></div>
        <?php if($model_app_view->comment && $model_app_view->app_status == 3): ?>
            <div class = "col-md-8 blk_info">
                <?= $model_app_view->comment ?>
            </div>
        <?php endif ?> 
    </div>
    
 <div class="row">
  <div class="col-md-10 row_block">
   <div class="col-md-12  block_btn">       
        <?php if($model_app_view->app_status != 2 && $model_app_view->app_status != 4 && $model_app_view->app_status != 5): ?>
    <div class="col-md-7">
        <?= Html::a('Изменить', ['update', 'id' => $model_app_view->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model_app_view->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить заявку ?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if($count_file == true): ?>
        <?= Html::a('Отправить заявку на рассмотрение', ['sendapp', 'id' => $model_app_view->id], ['class' => 'btn btn-primary']) ?>
        <?php endif ?>          
    </div>
    <div class="col-md-5 reg_btn"> 
         <?php if($model_app_view->customer_registr_number): ?>
         <div class="reg_num"> 
             <?= "№ ".$model_app_view->customer_registr_number ?>
         </div>     
        <?php endif ?>
      <?php
       Modal::begin([
        'header' => '<div class = "modal_header">Регистрация заявки</div>',
        'toggleButton' => [
        'label' => ($model_app_view->customer_registr_number) ? 'Изменить рег. номер' : 'Зарегистрировать заявку',
        'tag' => 'button',
        'class' => ($model_app_view->customer_registr_number) ? 'btn btn-link' : 'btn btn-success',
         ],
        'footer' => '',
       ]);
      ?>   
      <?php $form = ActiveForm::begin(['action' => ['app/application/customer-registration-number', 'id' => $_GET['id']]]); ?>
         <div class="modal-body">
            <div class="">   
            <?= $form->field($model_app_view, 'customer_registr_number')->textInput(['placeholder' => 'Внесите данные'],['maxlength' => true])->label('') ?>
            <?= $form->field($model_app_view, 'customer_registr_date')->input("date",['placeholder' => 'Дата документа'])->label('') ?> 
            </div>  
            <div class="">             
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div> 
         </div>      
      <?php ActiveForm::end(); ?> 
      <?php Modal::end(); ?>    
    </div>
   <?php endif ?>
   </div>     
  </div> 
</div> 
<div class="row">
   <div class="col-md-8 row_block">    
     <?php if($count_file != true): ?>
       <div class='row col-md-12 info_block_upl no'><h3>Загружены не все файлы</h3></div> 
     <?php endif ?> 
   </div>
</div>     
    <?//= DetailView::widget([
//        'model_app_view' => $model_app_view,
//        'attributes' => [
//            'id',
//            'land_category',
//            'permitted_use',
//            'reg_number',
//            'reg_date',
//            'extract_registry_number',
//            'extract_registry_date',
//            'target_servitut',
//            'title_org_servitut',
//            'term_date_with',
//            'term_date_by',
//            'area_servitut',
//            'scheme_servitut',
//            'report_date',
//            'pay_servitut',
//            'pay_period',
//            'date_preparation',
//            'amount_damages',
//            'loss_profits',
//            'reclamation_project',
//            'project_protocol',
//            'project_doc',
//            'reference_objects',
//            'reference_land',
//            'id_app_org',
//        ],
//    ]) ?>
       
    <div class="row">
    <div class="col col-md-8 content_application">        
        <div class = "title_application">ЗАЯВКА НА УСТАНОВЛЕНИЕ СЕРВИТУТА</div>
<div>
  <div class="form-group">
    <label for="">1. Наименование организации</label>
    <div><?= $name_org; ?></div>
  </div>
  <div class="col-md-12">
    <label for="">2. Сведения о земельном участке, на котором планируется установление 
сервитута:
    </label>  
  </div>
    <div class="col-md-12">     
      <label for=""><?//= $list_land1; ?></label>
    </div>
   <div class="form-group row">
    <div class="col-md-6">
      <label for="">- кадастровый номер;</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model_app_view->cadaster_number; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- адрес, место расположение;</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model_app_view->land_address; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- площадь (кв.м);</label>     
    </div>
    <div class="col-md-6">     
      <label for=""><?= $model_app_view->land_area; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- категория земель;</label>     
    </div>
    <div class="col-md-6">  
        <label for=""><?= $model_app_view->land->name_land; ?></label>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">- разрешенное использование;</label>     
    </div>
    <div class="col-md-6"> 
        <?= $model_app_view->permitted_use; ?>       
    </div>
  </div>
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- регистрационная запись государственной регистрации право собственности 
Российской Федерации;</label>
      <div class="col-md-6">
        <div class="form-check txt">
             <?= "№ ".$model_app_view->reg_number; ?>                
        </div>
        <div class="form-check txt">
            <?= $model_app_view->reg_date; ?> 
        </div>      
      </div>
    </div>
  </fieldset>
   <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- вещное право (постоянное (бессрочное), аренда);</label>
      <div class="col-md-6">
        <div class="form-check txt">
             <?= "№ ".$model_app_view->proprietary_reg_number; ?>                
        </div>
        <div class="form-check txt">
            <?= $model_app_view->proprietary_reg_date; ?> 
        </div> 
        <div class="form-check txt">
            <?= $model_app_view->proprietary_law; ?> 
        </div>  
      </div>
    </div>
  </fieldset>  
  <fieldset class="form-group col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">- выписка из единого государственного реестра недвижимости.</label>
      <div class="col-md-6">
        <div class="form-check txt">
            <?= "№ ".$model_app_view->extract_registry_number; ?> 
        </div>
        <div class="form-check txt">
            <?= $model_app_view->extract_registry_date; ?> 
        </div>      
      </div>
    </div>
  </fieldset>
  <div class="form-group row">  
     <div class="col-md-6">
        <label for="">3. Цель установления сервитута:</label>      
     </div> 
     <div class="col-md-6"> 
      <?= (!empty($model_app_view->other_data_servitut)) ? $model_app_view->other_data_servitut : $model_app_view->target->name_target; ?> 
     </div>
  </div>     
  <div class="form-group row">  
    <div class="col-md-6">
        <label for="">4. Наименовании организации, в интересах которой устанавливается 
        сервитут:
        </label>      
    </div>  
    <div class="col-md-6">
       <?= $model_app_view->title_org_servitut; ?>
    </div>
  </div>     
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">5. Срок установления сервитута.</label>
      <div class="col-md-6">
        <div class="form-check txt">
            <?= $model_app_view->term_date_with; ?>
        </div>
        <div class="form-check txt">
            <?= $model_app_view->term_date_by; ?>
        </div>      
      </div>
    </div>
  </fieldset> 
  <div class="form-group row">
    <div class="col-md-6">
      <label for="">6. Площадь сервитута (кв.м).</label>     
    </div>
    <div class="col-md-6"> 
         <?= $model_app_view->area_servitut; ?>
    </div>
  </div> 
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">7. Схема границ сервитута на кадастровом плане территории </label>
      <div class="col-md-6"> 
            <?= ($model_app_view->scheme_servitut == 1) ? 'представлена' : 'не представлена' ; ?>      
      </div>
    </div>
  </fieldset>
    
  <div class="row col-md-12">
    <label for="">8. Плата за сервитут:</label>     
  </div>     
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6">Отчет независимого оценщика:</label>
      <div class="col-md-6">
          <div class="form-check txt">
              <?= $model_app_view->org_appraiser; ?>
         </div>
         <div class="form-check txt">
              <?= "№ ".$model_app_view->appraiser_registry_number; ?>
         </div> 
         <div class="form-check txt">
              <?= $model_app_view->report_date; ?>
         </div>
        <div class="form-check txt">
            <?= $model_app_view->pay_servitut." руб."; ?>
        </div>
        <div class="form-check txt">
            <?= $model_app_view->pay_period; ?>
        </div>   
      </div>
    </div>
  </fieldset>
  <div class="row col-md-12">
    <label for="">9. Возмещение потерь и убытков:</label>     
  </div> 
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">отчет независимого оценщика:</label>
      <div class="col-md-6">
         <div class="form-check txt">
              <?= $model_app_view->date_preparation; ?>
         </div>
        <div class="form-check txt"> 
             <div class="col-md-8 blk_preparation">
                 <?= $model_app_view->amount_damages." руб."; ?>
             </div>
             <div class="col-md-4 blk_preparation">
                 <?= $model_app_view->damages_nds; ?>
             </div>    
        </div>
        <div class="form-check txt">
             <div class="col-md-8 blk_preparation">
                 <?= $model_app_view->loss_profits." руб."; ?>
             </div>
             <div class="col-md-4 blk_preparation">
                 <?= $model_app_view->loss_nds; ?>
             </div>
        </div>   
      </div>
    </div>
  </fieldset> 
   <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-sm-6 pt-0">10. Проект рекультивации земель</label>
      <div class="col-md-6">
            <?= ($model_app_view->reclamation_project == 1) ? 'представлен' : 'не представлен' ; ?>      
      </div>
    </div>
  </fieldset> 
   <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">11. Проект соглашения об установлении сервитута</label>
      <div class="col-md-6">
             <?= ($model_app_view->project_protocol == 1) ? 'представлен' : 'не представлен' ; ?>  
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">12. Проектная документация </label>
      <div class="col-md-6">
             <?= ($model_app_view->project_doc == 1) ? 'представлена' : 'не представлена' ; ?>
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">13. Информационная справка об объектах капитального строительства, 
        расположенных на земельном участке
      </label>
      <div class="col-md-6">
            <?= ($model_app_view->reference_objects == 1) ? 'представлена' : 'не представлена' ; ?> 
      </div>
    </div>
  </fieldset>
  <fieldset class="form-group row col-md-12">
    <div class="row">
      <label class="col-form-label col-md-6 pt-0">14. Информационная справка об использовании земельного участка</label>     
      <div class="col-md-6">  
            <?= ($model_app_view->reference_land == 1) ? 'представлена' : 'не представлена' ; ?>  
      </div>
    </div>
  </fieldset>       
   <div class="form-group row">  
  </div> 
</div>        
</div>    
</div> 
<?php if($count_file == true): ?>
 <div class='row col-md-8 info_block_upl'><h3>Все файлы к заявке загружены</h3></div> 
<?php else: ?>
 <div class='row col-md-8 info_block_upl no'><h3>Необходимо загрузить файлы</h3></div> 
<?php endif ?> 
  <div class="row file_upload col-md-8">
 <?php if($model_app_view->app_status != 2 && $model_app_view->app_status != 4 && $model_app_view->app_status != 5): ?>  
        <div class="col-md-8">
            <?= Html::a('Загрузка файлов', $url= 'index.php?r=app/application/upload-file&id_doc='.$_GET['id'], ['class' => 'btn btn-primary']) ?>      
        </div>
 <?php endif ?>        
        <?= "<div class='form-row col-md-12'>";  ?>        
        <?= "<div class='form-row file_upload_title col-md-12'><h3>Список загруженных файлов.</h3></div>";  ?>
     
    <?php $i = 1; ?>  
        <?php foreach ($name_doc_file AS $ndf){ ?> 
          <?= "<div class='col-md-1 fl'>".$i."</div>" ?> 
          <?= "<div class='col-md-11 fl'>". Html::a( $ndf->doc->name_doc, $url= $ndf->path). "</div>"  ?>          
        <?php $i++; ?>  
        <?php  } ?>       
        <?= "</div>";  ?>
    </div>       

</div>
  
