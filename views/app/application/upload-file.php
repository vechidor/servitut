<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title= "Загрузка файла";
$this->params['breadcrumbs'][] = ['label'=>'Заявка', 'url'=>['app/application/view' , 'id' => $id_doc]];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row col-md-12 content_upl_file">    
    <h4>Загрузка файла</h4>

    <?php $i = 1; ?>     
<?= "<div class='form-row col-md-12'>";  ?>      
    <?php foreach ($items_name_doc AS $imd){ ?> 
    
    <?php $form = ActiveForm::begin([
   // 'id' => 'doc-file',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-4\"></div><div class=\"col-lg-8\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-4 control-label']
    ]
]); ?>

    <?= "<div class='form-row col-md-12 blk_files'>";  ?> 
     <?= "<div class='form-row col-md-12'>";  ?>   
          <?= "<div class='col-md-1 fl'>".$i."</div>" ?>  
          <?= "<div class='col-md-11 fl'>". $imd->name_doc . "</div>"  ?> 
                    
    <?php foreach ($name_doc_file AS $ndf){ ?> 
       <?php  if($imd->id == $ndf->title){ ?>  
            <?= "<div class='col-md-1 fl'></div>"  ?> 
          <?= "<div class='col-md-9 fl'>". Html::a( $ndf->doc->name_doc, $url= $ndf->path). "</div>"  ?> 
          <?= "<div class='col-md-2 fl'>". Html::a('Удалить', ['filedelete', 'id' => $ndf->id, 'id_doc' => $id_doc, 'path_file' => $ndf->path], ['class' => 'btn btn-primary btn-sm'])."</div>"  ?>       
         <?php  } ?>   
    <?php  } ?> 
         
          <?= "<div class='col-md-10'>".$form->field($file_upload, 'fileDoc')->fileInput(['class' =>'btn btn-primary btn-sm']). "</div>"?>         
          <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-sm', 'name' => 'upload-file']) ?>
           
        <?= "</div>";  ?> 
          <?= $form->field($file, 'id_doc')->textInput(["value"=>$id_doc, "type"=>"hidden"]); ?>
          <?= $form->field($file, 'title')->textInput(["value"=>$imd->id, "type"=>"hidden"])->label(''); ?>
           <?php $i++; ?> 
     <?= "</div>";  ?> 
          <?php ActiveForm::end(); ?>
 <?php  } ?> 
<?= "</div>";  ?>
    

<?php if($count_file == true): ?>
    <div class="form-row col-md-12">        
            <div class='col-md-12 info_block_upl'><h3>Все файлы к заявке загружены</h3></div>           
<?php else: ?>
         <div class="form-row col-md-12 block_upl_file">

              
    <?php if(!empty($id_doc)): ?>
        <?//= $form->field($file_upload, 'fileDoc')->fileInput(['class' =>'btn btn-primary'])?>
            <div class="">
        <?//= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
    <?php else: ?>
            <div class='col-md-12'><h2>Заявка не создана</h2></div>       
    <?php endif ?> 
        </div>
<?php endif ?>        
    </div>               

</div> 