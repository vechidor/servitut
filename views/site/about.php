<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Информация';
?>
<div class="site-about">
    <h4><?= Html::encode($this->title) ?></h4>
    <h5>Рекомендованные интернет - браузеры:</h5>
    <p><a href="http://www.opera.com/ru/computer/windows" target="_blank">Opera</a></p>
    <p><a href="https://www.google.ru/chrome/browser/desktop/" target="_blank">Google Chrome</a></p>
    <p><a href="https://browser.yandex.ru/new/desktop/main/" target="_blank">Яндекс браузер</a></p>
    <hr>
    <h5>Техническая поддержка:</h5>
    <table class="table table-hover table-bordered">
        <tbody>
        <tr><th>Тема обращения</th><th>Телефон(ы)</th><th>E-mail</th></tr>
        </tbody>
        <tbody>
        <tr>
            <td>По техническим проблемам при работе с системой</td>
            <td>
                <p>8-495-989-84-47 (многоканальный)</p>
                <p>8(964)527-30-83</p>
            </td>
            <td><a href="mailto:ias@mirea.ru">ias@mirea.ru</a></td>
        </tr>
        </tbody>
    </table>
</div>
