<?php

use yii\db\Migration;

/**
 * Class m190725_042919_update_application_table
 */
class m190725_042919_update_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->execute("ALTER TABLE application modify reg_number varchar(255)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190725_042919_update_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190725_042919_update_application_table cannot be reverted.\n";

        return false;
    }
    */
}
