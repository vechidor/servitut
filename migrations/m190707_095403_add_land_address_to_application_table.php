<?php

use yii\db\Migration;

/**
 * Class m190707_095403_add_land_address_to_application_table
 */
class m190707_095403_add_land_address_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'land_address', $this->string(250)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'land_address');
    }
}
