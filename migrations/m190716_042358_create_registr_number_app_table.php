<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%registr_number_app}}`.
 */
class m190716_042358_create_registr_number_app_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%registr_number_app}}', [
            'id' => $this->primaryKey(),
            'registr_date' => $this->date(),
            'registr_number' => $this->string(300),
            'id_request' => $this->integer(11),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%registr_number_app}}');
    }
}
