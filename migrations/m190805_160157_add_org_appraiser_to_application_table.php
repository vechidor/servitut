<?php

use yii\db\Migration;

/**
 * Class m190805_160157_add_org_appraiser_to_application_table
 */
class m190805_160157_add_org_appraiser_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('application', 'org_appraiser', $this->string(250)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190805_160157_add_org_appraiser_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190805_160157_add_org_appraiser_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
