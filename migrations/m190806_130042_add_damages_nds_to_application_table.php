<?php

use yii\db\Migration;

/**
 * Class m190806_130042_add_damages_nds_to_application_table
 */
class m190806_130042_add_damages_nds_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'damages_nds', $this->string(50)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'damages_nds');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_130042_add_damages_nds_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
