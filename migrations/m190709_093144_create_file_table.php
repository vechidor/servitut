<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file}}`.
 */
class m190709_093144_create_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
            'id_doc' => $this->integer(),
            'id_type' => $this->integer(),
            'title' => $this->string(200),
            'path' => $this->string(500),
            'date_upload' => $this->date(),
            'delete' => $this->smallInteger(1)->defaultValue(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file}}');
    }
}
