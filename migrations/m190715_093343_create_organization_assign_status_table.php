<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organization_assign_status}}`.
 */
class m190715_093343_create_organization_assign_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%organization_assign_status}}', [
            'id' => $this->primaryKey(),
            'id_assign_status' => $this->integer(),
            'id_app' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%organization_assign_status}}');
    }
}
