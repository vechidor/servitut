<?php

use yii\db\Migration;

/**
 * Class m190806_084624_add_proprietary_reg_date_to_application_table
 */
class m190806_084624_add_proprietary_reg_date_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'proprietary_reg_date', $this->date()->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'proprietary_reg_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_084624_add_proprietary_reg_date_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
