<?php

use yii\db\Migration;

/**
 * Class m190809_094007_add_appraiser_registry_number_to_application_table
 */
class m190809_094007_add_appraiser_registry_number_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'appraiser_registry_number', $this->string(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190809_094007_add_appraiser_registry_number_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190809_094007_add_appraiser_registry_number_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
