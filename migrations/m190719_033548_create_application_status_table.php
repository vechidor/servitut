<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_status}}`.
 */
class m190719_033548_create_application_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_status}}', [
            'id' => $this->primaryKey(),
            'status_org' => $this->string(200),
        ]);
         $this->batchInsert('{{%application_status}}', ['status_org'], [
          ['Черновик'],
          ['На рассмотрении'],
          ['Отправлено на доработку'],
          ['Утверждено'],
          ['Отклонено'], 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%application_status}}');
    }
}
