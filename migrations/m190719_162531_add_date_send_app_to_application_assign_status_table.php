<?php

use yii\db\Migration;

/**
 * Class m190719_162531_add_date_send_app_to_application_assign_status_table
 */
class m190719_162531_add_date_send_app_to_application_assign_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application_assign_status', 'date_send_app', $this->date());      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190719_162531_add_date_send_app_to_application_assign_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190719_162531_add_date_send_app_to_application_assign_status_table cannot be reverted.\n";

        return false;
    }
    */
}
