<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%organization_status}}`.
 */
class m190719_040744_drop_organization_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%organization_status}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%organization_status}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
