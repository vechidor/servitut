<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%registr_number_app}}`.
 */
class m190724_162539_drop_registr_number_app_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%registr_number_app}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%registr_number_app}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
