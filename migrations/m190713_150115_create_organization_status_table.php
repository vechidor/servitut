<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organization_status}}`.
 */
class m190713_150115_create_organization_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%organization_status}}', [
            'id' => $this->primaryKey(),           
            'status_org' => $this->string(200),
        ]);
        $this->batchInsert('{{%organization_status}}', ['status_org'], [
          ['Черновик'],
          ['На рассмотрении'],
          ['Отправлено на доработку'],
          ['Утверждено'],
          ['Отклонено'], 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%organization_status}}');
    }
}
