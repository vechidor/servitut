<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%application_assign_status}}`.
 */
class m190724_161751_drop_application_assign_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%application_assign_status}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%application_assign_status}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
