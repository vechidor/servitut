<?php

use yii\db\Migration;

/**
 * Class m190806_084247_add_proprietary_reg_number_to_application_table
 */
class m190806_084247_add_proprietary_reg_number_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('application', 'proprietary_reg_number', $this->string(250)->defaultValue(NULL));   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'proprietary_reg_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_084247_add_proprietary_reg_number_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
