<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items_target}}`.
 */
class m190710_164521_create_items_target_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('{{%items_target}}', [
            'id' => $this->primaryKey(),
            'name_target' => $this->string(255)->notNull(),
        ]);
        
        $this->batchInsert('{{%items_target}}', ['name_target'], [
          ['Строительство'],
          ['Реконструкция'],
          ['Для обеспечения прохода и проезда'],
          ['Эксплуатации'],
          ['Иные'], 
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items_target}}');
    }
}
