<?php

use yii\db\Migration;

/**
 * Class m190720_184308_add_other_data_servitut_to_application_table
 */
class m190720_184308_add_other_data_servitut_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('application', 'other_data_servitut', $this->string(300));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190720_184308_add_other_data_servitut_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190720_184308_add_other_data_servitut_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
