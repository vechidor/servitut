<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181106_080903_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'id_org' => $this->integer()
        ]);
        $hash = Yii::$app->security->generatePasswordHash('password');
        $sql = "INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `id_org`, `name`) VALUES
                                  (1, 'admin@admin.ru', 'blTSGhhiHVA1T5bPEuL4xgViMIcWOZ3G', '$hash', NULL, 'admin@gek.ru', 10, 1533206471, 1533206471, 0, 'Админ')";
        $this->execute($sql);
    }
    public function down()
    {
        $userTable = Configs::instance()->userTable;
        $db = Configs::userDb();
        if ($db->schema->getTableSchema($userTable, true) !== null) {
            $this->dropTable($userTable);
        }
    }
}
