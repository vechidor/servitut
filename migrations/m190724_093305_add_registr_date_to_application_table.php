<?php

use yii\db\Migration;

/**
 * Class m190724_093305_add_registr_date_to_application_table
 */
class m190724_093305_add_registr_date_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('application', 'registr_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190724_093305_add_registr_date_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_093305_add_registr_date_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
