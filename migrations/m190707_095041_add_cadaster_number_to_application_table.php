<?php

use yii\db\Migration;

/**
 * Class m190707_095041_add_cadaster_number_to_application_table
 */
class m190707_095041_add_cadaster_number_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'cadaster_number', $this->string(50)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'cadaster_number');
    }

}
