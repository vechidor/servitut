<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_assign_status}}`.
 */
class m190719_033016_create_application_assign_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_assign_status}}', [
            'id' => $this->primaryKey(),
            'id_assign_status' => $this->integer(),
            'id_app' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%application_assign_status}}');
    }
}
