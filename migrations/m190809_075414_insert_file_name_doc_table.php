<?php

use yii\db\Migration;

/**
 * Class m190809_075414_insert_file_name_doc_table
 */
class m190809_075414_insert_file_name_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->batchInsert('file_name_doc', ['name_doc'], [
           ['Сопроводительное письмо']             
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190809_075414_insert_file_name_doc_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190809_075414_insert_file_name_doc_table cannot be reverted.\n";

        return false;
    }
    */
}
