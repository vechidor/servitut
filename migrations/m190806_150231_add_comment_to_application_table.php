<?php

use yii\db\Migration;

/**
 * Class m190806_150231_add_comment_to_application_table
 */
class m190806_150231_add_comment_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'comment', $this->string(300)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_150231_add_comment_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
