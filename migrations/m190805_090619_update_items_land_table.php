<?php

use yii\db\Migration;

/**
 * Class m190805_090619_update_items_land_table
 */
class m190805_090619_update_items_land_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->batchInsert('items_land', ['name_land'], [
           ['земли особо охраняемых территорий и объектов'],
           ['земли населенных пунктов'],
           ['земли сельскохозяйственного назначения'],
           ['земли промышленности, энергетики, транспорта, связи, радиовещания'],
           ['земли лесного фонда'],
           ['земли водного фонда'],
           ['земли запаса'],
           ['категория запаса'],     
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('items_land', ['name_land'], [
            ['Земли особо охраняемых территорий и объектов'],
            ['Земли населенных пунктов'],
            ['Земли сельскохозяйственного назначения'],
            ['Земли промышленности, энергетики, транспорта, связи, радиовещания'],
            ['Земли лесного фонда'],
            ['Земли водного фонда'],
            ['Земли запаса'],
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190805_090619_update_items_land_table cannot be reverted.\n";

        return false;
    }
    */
}
