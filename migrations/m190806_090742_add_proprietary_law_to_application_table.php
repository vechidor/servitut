<?php

use yii\db\Migration;

/**
 * Class m190806_090742_add_proprietary_law_to_application_table
 */
class m190806_090742_add_proprietary_law_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'proprietary_law', $this->string(50)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'proprietary_law');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190806_090742_add_proprietary_law_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
