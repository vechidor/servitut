<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application}}`.
 */
class m190705_074749_create_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('{{%application}}', [
            'id' => $this->primaryKey(),
            'date_creation' => $this->date(),
            'date_send' => $this->date(),
            'id_customer' => $this->integer(11),
            'delete_status' => $this->integer(11),
            'app_status' => $this->integer(11),
            'land_category' => $this->integer(11),
            'permitted_use' => $this->string(300),
            'reg_number' => $this->integer(11),
            'reg_date' => $this->date(),
            'extract_registry_number' => $this->integer(11),
            'extract_registry_date' => $this->date(),
            'target_servitut' => $this->string(300),
            'title_org_servitut' => $this->string(300),
            'term_date_with' => $this->date(),
            'term_date_by' => $this->date(),
            'area_servitut' => $this->integer(11),
            'scheme_servitut' => $this->integer(11),
            'report_date' => $this->date(),
            'pay_servitut' => $this->integer(11),
            'pay_period' => $this->string(50),
            'date_preparation' => $this->date(),
            'amount_damages' => $this->integer(50),
            'loss_profits' => $this->integer(50),
            'reclamation_project' => $this->integer(11),
            'project_protocol' => $this->integer(11),
            'project_doc' => $this->integer(11),
            'reference_objects' => $this->integer(11),
            'reference_land' => $this->integer(11),
            'id_app_org' => $this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%application}}');
    }
}
