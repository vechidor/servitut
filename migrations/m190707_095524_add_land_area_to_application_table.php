<?php

use yii\db\Migration;

/**
 * Class m190707_095524_add_land_area_to_application_table
 */
class m190707_095524_add_land_area_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'land_area', $this->string(50)->defaultValue(NULL));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'land_area');
    }
}
