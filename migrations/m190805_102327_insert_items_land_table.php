<?php

use yii\db\Migration;

/**
 * Class m190805_102327_insert_items_land_table
 */
class m190805_102327_insert_items_land_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('items_land', ['name_land'], [
           ['земли особо охраняемых территорий и объектов'],
           ['земли населенных пунктов'],
           ['земли сельскохозяйственного назначения'],
           ['земли промышленности, энергетики, транспорта, связи, радиовещания'],
           ['земли лесного фонда'],
           ['земли водного фонда'],
           ['земли запаса'],
           ['категория запаса'],     
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190805_102327_insert_items_land_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190805_102327_insert_items_land_table cannot be reverted.\n";

        return false;
    }
    */
}
