<?php

use yii\db\Migration;

/**
 * Class m190715_111613_add_id_land_to_application_table
 */
class m190715_111613_add_id_land_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->addColumn('application', 'id_land', $this->integer(50));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190715_111613_add_id_land_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190715_111613_add_id_land_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
