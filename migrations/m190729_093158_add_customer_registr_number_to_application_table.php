<?php

use yii\db\Migration;

/**
 * Class m190729_093158_add_customer_registr_number_to_application_table
 */
class m190729_093158_add_customer_registr_number_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'customer_registr_number', $this->string(300));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190729_093158_add_customer_registr_number_to_application_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190729_093158_add_customer_registr_number_to_application_table cannot be reverted.\n";

        return false;
    }
    */
}
