<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%organization_assign_status}}`.
 */
class m190719_041034_drop_organization_assign_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%organization_assign_status}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%organization_assign_status}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
