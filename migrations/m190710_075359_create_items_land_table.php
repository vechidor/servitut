<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%items_land}}`.
 */
class m190710_075359_create_items_land_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%items_land}}', [
            'id' => $this->primaryKey(),
            'name_land' => $this->string(255)->notNull(),
        ]);
        $this->batchInsert('{{%items_land}}', ['name_land'], [
          ['Земли особо охраняемых территорий и объектов'],
          ['Земли населенных пунктов'],
          ['Земли сельскохозяйственного назначения'],
          ['Земли промышленности, энергетики, транспорта, связи, радиовещания'],
          ['Земли лесного фонда'], 
          ['Земли водного фонда'],
          ['Земли запаса'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%items_land}}');
    }
}
