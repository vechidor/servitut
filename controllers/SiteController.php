<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use mdm\admin\models\User;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $user = User::findOne(Yii::$app->user->getId());
            if(!empty($user->id_org)){
                return $this->redirect(["app/application/index"]);
            }else{
                return $this->redirect(["app/admin-panel/index"]);
            }
        }

        $login = new LoginForm();
        if(isset($_GET['auth_token'])){
            $signer = new Sha256();
            $token = (new Parser())->parse($_GET['auth_token']);
            if($token->verify($signer, 'ias@mirea9884')){
                $login_remote =  trim($token->getClaim("login"));
                $password_remote =  trim($token->getClaim("password"));
                $login->username = $login_remote;
                $login->password = $password_remote;
                if($login->validate()){
                    Yii::$app->user->login($login->getUser());
                    return $this->redirect(["app/application/index"]);
                }else{
                    Yii::$app->session->setFlash("auth_error", "Ошибка входа!");
                }
            }
        }else{
            if($login->load(Yii::$app->request->post())){
                if($login->validate()){
                    Yii::$app->user->login($login->getUser());
                    $user = User::findOne(Yii::$app->user->getId());
                if(!empty($user->id_org)){   
                     return $this->redirect(["app/application/index"]);
                }else{
                     return $this->redirect(["app/admin-panel/index"]);
                }
                    
                }else{
                    Yii::$app->session->setFlash("auth_error", "Ошибка входа!");
                }

            }
        }
        return $this->render('login',compact('login'));
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionAbout()
    {
        return $this->render('about');
    }
}
