<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.11.2018
 * Time: 15:31
 */

namespace app\controllers\app;


class NewController extends AppController
{
    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}