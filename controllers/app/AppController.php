<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 01.08.2018
 * Time: 13:06
 */

namespace app\controllers\app;


use app\models\app\Organization;
use yii\web\Controller;
use Yii;
use mdm\admin\models\User;

abstract class AppController extends Controller
{

    public $layout;
    protected $id_org;
    protected $name_org;
    protected $id_user;

    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        if(!Yii::$app->user->isGuest){
            $user = User::findOne(Yii::$app->user->getId());
            if(!empty($user->id_org)){
                $this->id_user = $user->id;
                $this->id_org = $user->id_org;
                $this->name_org = Organization::findOne($user->id_org)->fullname;
            }
            $this->layout = 'app';
        }else{
            return $this->goHome();
        }

    }

    public function showError($mess="Ошибка доступа!"){
        Yii::$app->session->setFlash("error_message", $mess);
        return $this->render("@app/views/site/error");
    }
}