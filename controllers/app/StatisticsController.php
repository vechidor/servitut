<?php

namespace app\controllers\app;

class StatisticsController extends AppController
{
    public function actionGraphics()
    {
        return $this->render('graphics');
    }

}
