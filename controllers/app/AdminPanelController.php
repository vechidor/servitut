<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\app;

use Yii;
use app\models\app\Application;
use app\models\app\ApplicationSearch;
use app\models\app\file\File;
use app\models\app\FileDoc;
use app\models\app\FileType;
use app\models\app\ApplicationStatus;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\app;
use app\models\app\Organization;
use mdm\admin\models\User;
use \yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use app\models\UploadForm;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\models\app\ItemsLand;
use app\models\app\ItemsTarget;
use app\models\app\FileNameDoc;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use ZipArchive;
use yii\helpers\FileHelper;
/**
 * Description of Admin
 *
 * @author Serg
 */
class AdminPanelController extends AppController
{
    
    protected $id_user;
    protected $name_org;

    static $jwt_key = 'servitut_2019';
    
    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
                       
    }
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Application models.
     * @return mixed
     */
    public function actionIndex()
    {      
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->id_org);
       
        $filter = new Application();
        
        $items_so = ApplicationStatus::find()->where(['id' => [2,4,5]])->all();
        $items_status = ArrayHelper::map($items_so,'id','status_org');
        
        $items_t = ItemsTarget::find()->all();
        $items_target = ArrayHelper::map($items_t,'id','name_target');
        
        $all_app_org = Application::find()->where(['app_status' => [2,3,4,5], 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();
       
          
       if ($filter->load(Yii::$app->request->post())) { 
           
            if($filter['checkbox_status'] == true) {               
                $search_status = $filter['search_status'];           
                $all_app_org = Application::find()->where(['app_status' => $search_status, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();            
             }
        
            if($filter['checkbox_servitut'] == true) {
                $search_servitut = $filter['search_servitut'];
                $all_app_org = Application::find()->where(['app_status' => [2,3,4,5], 'target_servitut' => $search_servitut, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();        
             }
             
            if($filter['checkbox_org'] == true) {
              
                $search_org = $filter['checkbox_org'];       
                $all_app_org = Application::find()->where(['app_status' => [2,3,4,5], 'id_app_org' => $search_org, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();  
                
             }             
          }

             $requests_agenda = Application::find()->where(['app_status' => 2, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();
             $all_organization = Organization::find()->where(['id_founder' => 1, 'system_status' => 1])->orderBy(['id' => SORT_DESC])->all(); 

         return $this->render('index', [
            'searchModel' => $searchModel, 'filter' => $filter, 'items_status' => $items_status,
            'items_target' => $items_target, 'dataProvider' => $dataProvider, 'all_app_org' => $all_app_org, 'all_organization' => $all_organization,          
            'requests_agenda' => $requests_agenda]);       
    }
    
    public function actionClear()
    {
         $filter = new Application();
         if ($filter->load(Yii::$app->request->post())) { 
             $this->refresh();
         }
          return $this->redirect(['index']);
    }

    /**
     * Displays a single Application model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
         $model = $this->findModel($id);
        if(Yii::$app->user->can('read_request')){

             $items_so = ApplicationStatus::find()->where(['id' => [3,4,5]])->all();
             $items_status = ArrayHelper::map($items_so,'id','status_org');

             $name_doc_file =  File::find()->where(['id_doc'=>$id])->all();
                          
             $count_file_name_doc = FileNameDoc::find()->count();
             $count_file_app = File::find()->where(['id_doc' => $id]) ->count();
             
                if($count_file_name_doc <= $count_file_app){
                    $count_file = true;
                }else{
                    $count_file = false;
                }

             return $this->render('view', [
            'model' => $this->findModel($id), 'items_status' => $items_status,            
            'name_doc_file' => $name_doc_file, 'count_file' => $count_file ]);                                     
        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }
    }
    
    public function actionPrintPutAgenda(){
         $filter = new Application();
       
         if ($filter->load(Yii::$app->request->post())) { 

            header("Content-type: application/msword");
            header("Content-Disposition: attachment; filename=Печатная версия.doc");
            header("Content-Transfer-Encoding: binary");
            $this->layout = 'print-doc';
            
              $options_agenda = Application::find()->where([ 'id'=> $filter['checkbox_app_org'], 'app_status' => 2, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();
        
              }
        
        return $this->renderPartial('print-put-agenda', compact('options_agenda'));
    }
    
     function actionPrintOrgReport($id){

            header("Content-type: application/msword");
            header("Content-Disposition: attachment; filename=Печатная версия.doc");
            header("Content-Transfer-Encoding: binary");
            $this->layout = 'print-doc';

        $options = Application::find()->where(['id'=>$id, 'delete_status'=>1])->one();
 
        return $this->renderPartial('print-docx', compact('options'));
    }
    
    
    public function actionChangeStatus($id)
    {
             $model = new Application();

            if ($model->load(Yii::$app->request->post())) {   
                Yii::$app->db->createCommand()->update('application', ['app_status' => $model['app_status']], ['id' => $id])->execute(); 
            if($model['app_status']){                 
                    Yii::$app->session->setFlash("success", "Статус сохранен");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при сохранении статуса!");
                }          
             }  
            
               return $this->redirect(['view', 'id' => $id]);
             
    }
    
     public function actionComment($id)
    {
             $model = new Application();

            if ($model->load(Yii::$app->request->post())) {   
                Yii::$app->db->createCommand()->update('application', ['comment' => $model['comment']], ['id' => $id])->execute(); 
            if($model['comment']){                 
                    Yii::$app->session->setFlash("success", "Комментарий сохранен");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при сохранении комментария!");
                }          
             }  
            
               return $this->redirect(['view', 'id' => $id]);
             
    }
    
    public function actionRegistrationNumber($id)
    {
             $model = new Application();
 
            if ($model->load(Yii::$app->request->post())) {   
                Yii::$app->db->createCommand()->update('application', ['registr_number' => $model['registr_number'], 'registr_date' => $model['registr_date']], ['id' => $id])->execute(); 
            if($model['registr_number']){                 
                    Yii::$app->session->setFlash("success", "Номер и дата сохранены");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при сохранении номера и даты!");
                }          
             }             
               return $this->redirect(['view', 'id' => $id]);             
    }
 
     //загрузка файла для документа
    public function actionUploadFile($id_doc){
        
        if(Yii::$app->user->can('read_request')){ 
             $file = new File();
             $file_type = FileType::find()->where(['safe'=>1])->all();
             $type = [];
              foreach ($file_type AS $ft){
                $type[$ft['id']] =$ft['type'];
              }       
             $items_name_doc = FileNameDoc::find()->all();
             $name_doc = ArrayHelper::map($items_name_doc,'id','name_doc');
    
             $name_doc_file =  File::find()->where(['id_doc'=>$id_doc])->all();
 
             $file_upload = new FileDoc();
    
        if($file->load(Yii::$app->request->post())){
            $file_upload->fileDoc = UploadedFile::getInstance($file_upload, 'fileDoc');
            if($this->id_org){
            $result_upload = $file_upload->upload($this->id_org, $file->id_doc);
            }else{
              $id_doc_org = Application::find()->where(['id' => $id_doc])->one();  
              $id_org = $id_doc_org['id_app_org'];  
            $result_upload = $file_upload->upload($id_org, $file->id_doc);   
            }
            if ($result_upload===false) {
                Yii::$app->session->setFlash("error", "Ошибка при загрузке файла!", false);
                return $this->refresh();
            }else{
                $file->path = $result_upload;
                $file->date_upload = date("Y-m-d");
                if($file->save()){
                    Yii::$app->session->setFlash("success", "Файл загружен");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при добавлении информации!");
                }
            }
        }
        
             $count_file_name_doc = FileNameDoc::find()->count();
             $count_file_app = File::find()->where(['id_doc' => $id_doc]) ->count();
             
                if($count_file_name_doc <= $count_file_app){
                    $count_file = true;
                }else{
                    $count_file = false;
                }
        
            return $this->render('upload-file', compact("id_doc","file", "type", "file_upload", "name_doc_file", "name_doc", "count_file"));
        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }  
        
        }
        
    public function actionZipDownload($id_org, $id_doc){

        $dir = "uploads/{$id_org}/{$id_doc}/"; 
        $fileName = 'ZipDoc-'.$id_org.'-'.$id_doc.'.zip'; 
        $files= FileHelper::findFiles($dir); 
         
         $zip = new ZipArchive();
        
        if ($zip->open($fileName, ZIPARCHIVE::CREATE) !== true) {
            fwrite(STDERR, "Error while creating archive file");
   
        }

        foreach ($files as $file) {
              $name = substr($file, strrpos($file, '/') + 1);             
              $zip->addFile($dir.$name, $name);
            }

        $zip->close();
    
        if(file_exists($fileName)) {

            header('Content-type: application/zip');
            header('Content-Disposition: attachment; filename="'.$fileName.'"');
            readfile($fileName);

            unlink($fileName);

         }else{
            Yii::$app->session->setFlash("error", "Архив не создан !");
        }

         return $this->redirect(['view', 'id' => $id]);    
 }    
        
    public function actionFiledelete($id, $id_doc, $path_file){
        
         $model = File::find()->where(['id' => $id])->one();
         $model->delete();
         unlink($path_file);
         return $this->redirect(['upload-file', 'id_doc' => $id_doc]);

    }
    
    public function actionStatistics(){
        
        $model_graphics = new Application();
        
         $quantitative_approved_app = Application::find()->select(['land_area'])->where(['app_status' => 4, 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->column();            
        
        $data['data'] = json_encode($quantitative_approved_app);
        
      
        return $this->render('statistics', ['data' => $data, 'ggg' => $ggg]);
    }
   
    /**
     * Finds the Application model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Application the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
