<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\app;

use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Description of UserController
 *
 * @author Serg
 */
class UserController extends ActiveController
{
   
     public function behaviors()
    {
        return [
       
        ContentNegotiator => [    
            'class' => \yii\filters\ContentNegotiator::class,
            'formatParam' => 'format',
            'formats' => ['json' => Response::FORMAT_JSON ]
            
            ]
            ];
   
        
        
    }
    
    
    public $modelClass = 'app\models\User';
}
