<?php

namespace app\controllers\app;

use Yii;
use app\models\app\Application;
use app\models\app\ApplicationSearch;
use app\models\app\file\File;
use app\models\app\FileDoc;
use app\models\app\FileType;
use app\models\app\ApplicationStatus;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\app;
use app\models\app\Organization;
use mdm\admin\models\User;
use \yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use app\models\UploadForm;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\models\app\ItemsLand;
use app\models\app\ItemsTarget;
use app\models\app\FileNameDoc;
use yii\helpers\ArrayHelper;

/**
 * ApplicationController implements the CRUD actions for Application model.
 */
class ApplicationController extends AppController
{
    
    protected $id_user;
    protected $name_org;

    static $jwt_key = 'servitut_2019';
    
    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);            
                      
    }
    
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Application models.
     * @return mixed
     */
    public function actionIndex()
    {            
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->id_org);
    
        $all_app = Application::find()->where(['id_app_org' => $this->id_org, 'app_status' => [1,2,3,4,5], 'delete_status'=>1])->orderBy(['id' => SORT_DESC])->all();
 
            return $this->render('index', [
                 'searchModel' => $searchModel,
                 'dataProvider' => $dataProvider, 'name_org' => $this->name_org,
                 'all_app' => $all_app,                
         ]); 
    }

    /**
     * Displays a single Application model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
         $model_app_view = $this->findModel($id);
        if(Yii::$app->user->can('read_request')){
             
             $name_doc_file =  File::find()->where(['id_doc'=>$id])->all();

             $count_file_name_doc = FileNameDoc::find()->count();
             $count_file_app = File::find()->where(['id_doc' => $id]) ->count();
             
                if($count_file_name_doc <= $count_file_app){
                    $count_file = true;
                }else{
                    $count_file = false;
                }     
             return $this->render('view', [
            'model_app_view' => $this->findModel($id), 'name_org' => $this->name_org,            
            'name_doc_file' => $name_doc_file,'count_file' => $count_file,
            ]);
        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }
    }
    
     public function actionCustomerRegistrationNumber($id)
    {
             $model_app_view = new Application();
 
            if ($model_app_view->load(Yii::$app->request->post())) {   
                Yii::$app->db->createCommand()->update('application', ['customer_registr_number' => $model_app_view['customer_registr_number'],
                'customer_registr_date' => $model_app_view['customer_registr_date']], ['id' => $id])->execute(); 
            if($model_app_view['customer_registr_number']){                 
                    Yii::$app->session->setFlash("success", "Номер и дата сохранены");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при сохранении номера и даты!");
                }          
             }             
               return $this->redirect(['view', 'id' => $id]);             
    }
    /**
     * Creates a new Application model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {       
        $model = new Application();
        if(Yii::$app->user->can('create_request')){

            $model->id_customer = $this->id_user;
            $model->date_creation = date("Y-m-d H:i:s");
            $model->id_app_org = $this->id_org;
            $model->app_status = 1;
            $model->delete_status = 1;

            //формируем запрос в системе ИАС Мониторинг для получения списка земель
            $signer = new Sha256();
            $token = (new Builder())
                ->set('id_org', $this->id_org)
                ->set('type_request', 'get_all')
                ->sign($signer, self::$jwt_key)
                ->getToken();

            $response_token = file_get_contents("http://api.xn--80apneeq.xn--p1ai/api.php?option=land_api&action=get_land&module=servitut&token=$token");

            $signer = new Sha256();
            $token = (new Parser())->parse($response_token);
            if($token->verify($signer, self::$jwt_key)){

                $data_reference = $token->getClaims();
                $list_land = [];
                foreach ($data_reference AS $key=>$data){
                    $list_land[$data->getValue()->id] = $data->getValue()->address;
                }
            }else{
                Yii::$app->session->setFlash("error", "Неверный токен получения справочника!");
            }
               
               $items_t = ItemsTarget::find()->all();
               $items_target = ArrayHelper::map($items_t,'id','name_target');
                             
                      
            if ($model->load(Yii::$app->request->post())) {
                
                //запрашиваем данные по определенному земельному участку (выбранному в форме)
                $signer = new Sha256();
                $token = (new Builder())
                    ->set('id_row', $model->id_land)
                    ->set('type_request', 'get_one')
                    ->sign($signer, self::$jwt_key)
                    ->getToken();

                $response_token = file_get_contents("http://api.xn--80apneeq.xn--p1ai/api.php?option=land_api&action=get_land&module=servitut&token=$token");

                $signer = new Sha256();
                $token = (new Parser())->parse($response_token);
                if($token->verify($signer, self::$jwt_key)){

                    $data = $token->getClaims();
                    $model->cadaster_number = $data[0]->getValue()->kadastr_nomer;
                    $model->land_address = $data[0]->getValue()->address;
                    $model->land_area = $data[0]->getValue()->area;
                    $model->land_category = $data[0]->getValue()->id_category_land;
                    if(!$model->save()){
                        var_dump($model->getErrors());
                        Yii::$app->session->setFlash("error", "Произошла ошибка при сохранении!");
                    }

                }else{
                    Yii::$app->session->setFlash("error", "Неверный токен получения справочника!");
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
                return $this->render('create', [
                    'model' => $model, 'name_org' => $this->name_org,
                  'list_land'=>$list_land, 'items_target'=> $items_target,
                   
                 ]);
        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }    
    }
    
    /**
     * Updates an existing Application model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
 
       if(Yii::$app->user->can('update_request')){
           
           //формируем запрос в системе ИАС Мониторинг для получения списка земель
            $signer = new Sha256();
            $token = (new Builder())
                ->set('id_org', $this->id_org)
                ->set('type_request', 'get_all')
                ->sign($signer, self::$jwt_key)
                ->getToken();

            $response_token = file_get_contents("http://api.xn--80apneeq.xn--p1ai/api.php?option=land_api&action=get_land&module=servitut&token=$token");

            $signer = new Sha256();
            $token = (new Parser())->parse($response_token);
            if($token->verify($signer, self::$jwt_key)){

               $data_reference = $token->getClaims();
               $list_land = [];
                foreach ($data_reference AS $key=>$data){
                    $list_land[$data->getValue()->id] = $data->getValue()->address;
                }
            }else{
                Yii::$app->session->setFlash("error", "Неверный токен получения справочника!");
            }
            
             $items_t = ItemsTarget::find()->all();
             $items_target = ArrayHelper::map($items_t,'id','name_target');             
           
        if ($model->load(Yii::$app->request->post())) {
            
             //запрашиваем данные по определенному земельному участку (выбранному в форме)
                $signer = new Sha256();
                $token = (new Builder())
                    ->set('id_row', $model->id_land)
                    ->set('type_request', 'get_one')
                    ->sign($signer, self::$jwt_key)
                    ->getToken();

                $response_token = file_get_contents("http://api.xn--80apneeq.xn--p1ai/api.php?option=land_api&action=get_land&module=servitut&token=$token");

                $signer = new Sha256();
                $token = (new Parser())->parse($response_token);
                if($token->verify($signer, self::$jwt_key)){

                    $data = $token->getClaims();
                    $model->cadaster_number = $data[0]->getValue()->kadastr_nomer;
                    $model->land_address = $data[0]->getValue()->address;
                    $model->land_area = $data[0]->getValue()->area;
                    $model->land_category = $data[0]->getValue()->id_category_land;              
                    if(!$model->save()){
                        var_dump($model->getErrors());
                        Yii::$app->session->setFlash("error", "Произошла ошибка при сохранении!");
                    }
                }else{
                    Yii::$app->session->setFlash("error", "Неверный токен получения справочника!");
                }
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

            return $this->render('update', [
            'model' => $model, 'name_org' => $this->name_org,
                    'list_land'=>$list_land, 'items_target' => $items_target,
                   
             ]);
       }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
       }
    }

    /**
     * Deletes an existing Application model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('delete_request')){
          Yii::$app->db->createCommand()->update('application', ['delete_status' => 0], ['id' => $id])->execute();
          return $this->redirect(['index']);
        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }  
    }

     //загрузка файла для документа
    public function actionUploadFile($id_doc){
       
        if(Yii::$app->user->can('read_request')){ 
             $file = new File();
             $file_type = FileType::find()->where(['safe'=>1])->all();
             $type = [];
                foreach ($file_type AS $ft){
                     $type[$ft['id']] =$ft['type'];
                 }       
             $items_name_doc = FileNameDoc::find()->all();
                
             $name_doc_file =  File::find()->where(['id_doc'=>$id_doc])->all();
 
             $file_upload = new FileDoc();
    
        if($file->load(Yii::$app->request->post())){
            $file_upload->fileDoc = UploadedFile::getInstance($file_upload, 'fileDoc');
            if($this->id_org){
                 $result_upload = $file_upload->upload($this->id_org, $file->id_doc);
            }else{
                 $id_doc_org = Application::find()->where(['id' => $id_doc])->one();  
                 $id_org = $id_doc_org['id_app_org'];  
                 $result_upload = $file_upload->upload($id_org, $file->id_doc);   
            }
            if ($result_upload===false) {
                Yii::$app->session->setFlash("error", "Ошибка при загрузке файла!", false);
                return $this->refresh();
            }else{
                $file->path = $result_upload;
                $file->date_upload = date("Y-m-d");
                if($file->save()){
                    Yii::$app->session->setFlash("success", "Файл загружен");
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash("error", "Ошибка при добавлении информации!");
                }
            }
        }
        
            $count_file_name_doc = FileNameDoc::find()->count();
            $count_file_app = File::find()->where(['id_doc' => $id_doc]) ->count(); 
            
                if($count_file_name_doc <= $count_file_app){
                    $count_file = true;
                }else{
                    $count_file = false;
                }
            return $this->render('upload-file', compact("id_doc","file", "type", "file_upload", 
                                 "name_doc_file", "name_doc", "count_file", "count_file", 'items_name_doc'));

        }else{
           throw new ForbiddenHttpException("Доступ запрещен !");
        }  
        
        }
        
    public function actionFiledelete($id, $id_doc, $path_file){
        
         $model = File::find()->where(['id' => $id])->one();
         $model->delete();
         unlink($path_file);
         
         return $this->redirect(['upload-file', 'id_doc' => $id_doc]);
    }
    
    public function actionSendapp($id)
    {
    
        $model = $this->findModel($id); 
        $date_send = date("Y-m-d H:i:s");
        
        if($model->customer_registr_number != null){
           Yii::$app->db->createCommand()->update('application', ['app_status' => 2, 'date_send' => $date_send], ['id' => $id])->execute(); 
            if($model->app_status){             
                  Yii::$app->session->setFlash("success", "Заявка отправлена на рассмотрение");
             }else{
                  Yii::$app->session->setFlash("error", "Ошибка при сохранении статуса!");                
             }
        }else{
            Yii::$app->session->setFlash("error", "Необходимо зарегистрировать заявку!");  
        }     
             return $this->redirect(['view', 'id' => $id]);
    }
    
    /**
     * Finds the Application model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Application the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

