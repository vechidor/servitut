<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 01.08.2018
 * Time: 17:36
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // добавляем разрешение "create"
        $create = $auth->createPermission('create');
        $create->description = 'Create';
        $auth->add($create);

        // добавляем разрешение "read"
        $read = $auth->createPermission('read');
        $read->description = 'read';
        $auth->add($read);

        // добавляем разрешение "update"
        $update = $auth->createPermission('update');
        $update->description = 'Update';
        $auth->add($update);

        // добавляем разрешение "delete"
        $delete = $auth->createPermission('delete');
        $delete->description = 'delete';
        $auth->add($delete);

        // добавляем роль "user" и даём роли разрешение "create", "read"
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $create);
        $auth->addChild($user, $read);

        // добавляем роль "admin" и даём роли разрешение "update", "delete"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $update);
        $auth->addChild($admin, $delete);
        $auth->addChild($admin, $user);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($user, 2);
        $auth->assign($admin, 1);
    }
}