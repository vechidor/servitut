<?php

namespace app\models\app;

use Yii;
use \yii\db\ActiveRecord;
/**
 * This is the model class for table "organization".
 *
 * @property int $id
 * @property string $fullname
 * @property string $short_name
 * @property string $name
 * @property string $ogrn
 * @property string $inn
 * @property string $addresses
 * @property int $id_type
 * @property int $id_founder
 * @property int $id_region
 * @property string $org_num
 * @property int $system_status 1-активная организация, 0- удаленная
 *
 * @property OrganizationFounder $founder
 * @property OrganizationType $type
 * @property OrganizationType $type0
 * @property User[] $users
 */
class Organization extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization';
    }
     public function getOrg()
    {
        return $this->hasMany(Application::className(), ['id_app_org' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_type', 'id_founder', 'id_region'], 'required'],
            [['id_type', 'id_founder', 'id_region', 'system_status'], 'integer'],
            [['fullname'], 'string', 'max' => 400],
            [['short_name'], 'string', 'max' => 150],
            [['name'], 'string', 'max' => 300],
            [['ogrn', 'inn'], 'string', 'max' => 50],
            [['addresses'], 'string', 'max' => 250],
            [['org_num'], 'string', 'max' => 80],
            [['id_founder'], 'exist', 'skipOnError' => true, 'targetClass' => OrganizationFounder::className(), 'targetAttribute' => ['id_founder' => 'id']],
            [['id_type'], 'exist', 'skipOnError' => true, 'targetClass' => OrganizationType::className(), 'targetAttribute' => ['id_type' => 'id']],
            [['id_type'], 'exist', 'skipOnError' => true, 'targetClass' => OrganizationType::className(), 'targetAttribute' => ['id_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'short_name' => 'Short Name',
            'name' => 'Name',
            'ogrn' => 'Ogrn',
            'inn' => 'Inn',
            'addresses' => 'Addresses',
            'id_type' => 'Id Type',
            'id_founder' => 'Id Founder',
            'id_region' => 'Id Region',
            'org_num' => 'Org Num',
            'system_status' => 'System Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFounder()
    {
        return $this->hasOne(OrganizationFounder::className(), ['id' => 'id_founder']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OrganizationType::className(), ['id' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(OrganizationType::className(), ['id' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_org' => 'id']);
    }
}
