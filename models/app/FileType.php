<?php

namespace app\models\app;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "file_type".
 *
 * @property int $id
 * @property string $type
 * @property string $mime
 * @property int $safe
 * @property string $img
 */
class FileType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['safe'], 'integer'],
            [['type'], 'string', 'max' => 20],
            [['mime'], 'string', 'max' => 100],
            [['img'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'mime' => 'Mime',
            'safe' => 'Safe',
            'img' => 'Img',
        ];
    }
}
