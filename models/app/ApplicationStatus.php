<?php

namespace app\models\app;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "application_status".
 *
 * @property int $id
 * @property string $status_org
 */
class ApplicationStatus extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_status';
    }
     public function getStatus()
    {
        return $this->hasOne(Application::className(), ['app_status' => 'id']);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_org'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_org' => 'Status Org',
        ];
    }
}
