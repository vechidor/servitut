<?php

namespace app\models\app;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\app\Application;

/**
 * ApplicationSearch represents the model behind the search form of `app\models\Application`.
 */
class ApplicationSearch extends Application
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reg_number', 'extract_registry_number', 'area_servitut', 'scheme_servitut', 'pay_servitut', 'amount_damages', 'loss_profits', 'reclamation_project', 'project_protocol', 'project_doc', 'reference_objects', 'reference_land', 'id_app_org'], 'integer'],
            [['land_category', 'permitted_use', 'reg_date', 'extract_registry_date', 'target_servitut', 'title_org_servitut', 'term_date_with', 'term_date_by', 'report_date', 'pay_period', 'date_preparation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id_app_org = null)
    {
        $query = Application::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reg_number' => $this->reg_number,
            'reg_date' => $this->reg_date,
            'extract_registry_number' => $this->extract_registry_number,
            'extract_registry_date' => $this->extract_registry_date,
            'term_date_with' => $this->term_date_with,
            'term_date_by' => $this->term_date_by,
            'area_servitut' => $this->area_servitut,
            'scheme_servitut' => $this->scheme_servitut,
            'report_date' => $this->report_date,
            'pay_servitut' => $this->pay_servitut,
            'date_preparation' => $this->date_preparation,
            'amount_damages' => $this->amount_damages,
            'loss_profits' => $this->loss_profits,
            'reclamation_project' => $this->reclamation_project,
            'project_protocol' => $this->project_protocol,
            'project_doc' => $this->project_doc,
            'reference_objects' => $this->reference_objects,
            'reference_land' => $this->reference_land,
            'id_app_org' => $id_app_org,
        ]);

        $query->andFilterWhere(['like', 'land_category', $this->land_category])
            ->andFilterWhere(['like', 'permitted_use', $this->permitted_use])
            ->andFilterWhere(['like', 'target_servitut', $this->target_servitut])
            ->andFilterWhere(['like', 'title_org_servitut', $this->title_org_servitut])
            ->andFilterWhere(['like', 'pay_period', $this->pay_period]);

        return $dataProvider;
    }
}
