<?php

namespace app\models\app\file;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\models\app\FileNameDoc;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property int $id_doc
 * @property int $id_type
 * @property string $title
 * @property string $path
 * @property string $date_upload
 * @property int $delete
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }
     public function getDoc()
    {
        return $this->hasOne(FileNameDoc::className(), ['id' => 'title']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_doc', 'id_type', 'delete'], 'integer'],
            [['date_upload'], 'safe'],
            [['title'], 'string', 'max' => 200],
            [['title'] , 'required'],
            [['path'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_doc' => '',
            'id_type' => 'Id Type',
            'title' => 'Документ',
            'path' => 'Path',
            'date_upload' => 'Date Upload',
            'delete' => 'Delete',
        ];
    }
}
