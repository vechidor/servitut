<?php

namespace app\models\app;

use Yii;

/**
 * This is the model class for table "items_target".
 *
 * @property int $id
 * @property string $name_target
 */
class ItemsTarget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items_target';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_target'], 'required'],
            [['name_target'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_target' => 'Name Target',
        ];
    }
}
