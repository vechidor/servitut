<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\app;

use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class FileDoc extends Model
{
    /**
     * @var UploadedFile
     */
    public $fileDoc;

    public function rules()
    {
        return [
            [['fileDoc'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf', 'maxSize'=>(10*1024*1024)],
        ];
    }

    public function attributeLabels(){
        return ["fileDoc"=>"Файл для загрузки"];
    }


    public function upload($id_org, $id_doc)
    {
        $today = date("Y-m-d_H-i-s");
        if ($this->validate()) {
            $path = "uploads/{$id_org}/{$id_doc}/";
            FileHelper::createDirectory($path);
            if($this->fileDoc->saveAs($path . $today.$this->fileDoc->baseName . '.' . $this->fileDoc->extension)){
                return $path . $today.$this->fileDoc->baseName . '.' . $this->fileDoc->extension;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
}