<?php

namespace app\models\app;

use Yii;
use \yii\db\ActiveRecord;
/**
 * This is the model class for table "application".
 *
 * @property int $id
 * @property string $date_creation
 * @property string $date_send
 * @property int $id_customer
 * @property int $delete_status
 * @property int $app_status
 * @property int $land_category
 * @property string $permitted_use
 * @property int $reg_number
 * @property string $reg_date
 * @property int $extract_registry_number
 * @property string $extract_registry_date
 * @property string $target_servitut
 * @property string $title_org_servitut
 * @property string $term_date_with
 * @property string $term_date_by
 * @property int $area_servitut
 * @property int $scheme_servitut
 * @property string $report_date
 * @property int $pay_servitut
 * @property string $pay_period
 * @property string $date_preparation
 * @property int $amount_damages
 * @property int $loss_profits
 * @property int $reclamation_project
 * @property int $project_protocol
 * @property int $project_doc
 * @property int $reference_objects
 * @property int $reference_land
 * @property int $id_app_org
 * @property string $cadaster_number
 * @property string $land_address
 * @property string $land_area
 * @property int $id_land
 */
class Application extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application';
    }
    public function getOrg()
    {
        return $this->hasOne(Organization::className(), ['id' => 'id_app_org']);
    }
     public function getNum()
    {
        return $this->hasOne(RegistrNumberApp::className(), ['id_request' => 'id']);
    }
     public function getLand()
    {
        return $this->hasOne(ItemsLand::className(), ['id' => 'land_category']);
    }
     public function getTarget()
    {
        return $this->hasOne(ItemsTarget::className(), ['id' => 'target_servitut']);
    }
    public function getStatus()
    {
        return $this->hasOne(ApplicationStatus::className(), ['id' => 'app_status']);
    }
    /**
     * {@inheritdoc}
     */
    public $checkbox_status;
    public $checkbox_servitut;
    public $checkbox_org;
    public $search_status;
    public $search_servitut;
  //  public $search_org; 
    public $checkbox_app_org;
    public function rules()
    {
        return [
            
            [['checkbox_status', 'checkbox_servitut', 'checkbox_org', 'search_status', 'search_servitut', /*'search_org',*/ 'checkbox_app_org' ], 'safe'],
            [['area_servitut', 'scheme_servitut', 'pay_servitut', 'amount_damages', 'loss_profits', 'reclamation_project', 'project_protocol', 'project_doc', 'reference_objects', 'reference_land', 'id_app_org'], 'number'],
                       
            [['reg_number', 'extract_registry_number', 'area_servitut', 'scheme_servitut', 
              'pay_servitut', 'amount_damages', 'loss_profits', 'reclamation_project', 'project_protocol',
              'project_doc', 'reference_objects', 'reference_land', 'reg_date', 'extract_registry_date', 
              'term_date_with', 'term_date_by', 'report_date', 'date_preparation', 'land_category', 'permitted_use', 
              'target_servitut', 'title_org_servitut', 'pay_period', 'id_land', 'app_status', 'org_appraiser', 'proprietary_reg_number', 'proprietary_reg_date', 'proprietary_law', 'damages_nds', 'loss_nds', 'appraiser_registry_number'], 'required'],
                       
             ['search_status', 'required', 'when' => function ($model) {
                 return $model->search_status == true;
             },'whenClient' => "function (attribute, value) {
                 return $('#application-checkbox_status').attr('checked') == 'checked';
            }"] ,
                     
             ['search_servitut', 'required', 'when' => function ($model) {
                 return $model->search_servitut == true;
             },'whenClient' => "function (attribute, value) {
                 return $('#application-checkbox_servitut').attr('checked') == 'checked';
            }"] , 
                     
//            ['search_org', 'required', 'when' => function ($model) {
//                 return $model->search_org == true;
//             },'whenClient' => "function (attribute, value) {
//                 return $('#application-checkbox_org').attr('checked') == 'checked';
//            }"] ,              
            
            ['other_data_servitut', 'required', 'when' => function ($model) {
                 return $model->other_data_servitut == '5';
             },'whenClient' => "function (attribute, value) {
                 return $('#application-target_servitut').val() == '5';
            }"] ,                        
                         
            [['reg_date', 'extract_registry_date', 'term_date_with', 'term_date_by', 'report_date', 'date_preparation', 'registr_date', 'customer_registr_date', 'proprietary_reg_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['land_category', 'permitted_use', 'target_servitut', 'title_org_servitut'], 'string', 'max' => 300],
            [['pay_period'], 'string', 'max' => 50],
            [['reg_number', 'extract_registry_number', 'cadaster_number','land_address',
              'land_area', 'registr_number', 'customer_registr_number', 'org_appraiser', 
              'proprietary_reg_number', 'proprietary_law', 'damages_nds', 'loss_nds', 'comment', 'appraiser_registry_number'], 'string'],
            [['id_land', 'app_status'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_creation' => 'Дата создания',
            'date_send' => 'Дата отправки',
            'id_customer' => 'Id пользователя',
            'delete_status' => 'Маркер логического удаления',
            'app_status' => 'Индентификатор принадлежности к конкретной организации',
            'land_category' => '- категория земель',
            'permitted_use' => '- разрешенное использование',
            'reg_number' => 'Номер регистрационной записи',
            'reg_date' => 'Дата регистрационной записи',
            'extract_registry_number' => 'Номер выписки из единого государственного реестра недвижимости',
            'extract_registry_date' => 'Дату выписки из единого государственного реестра недвижимости',
            'target_servitut' => 'Цель установления сервитута',
            'title_org_servitut' => 'Наименовании организации, в интересах которой устанавливается сервитут',
            'term_date_with' => 'Срок установления сервитута (дата с...)',
            'term_date_by' => 'Срок установления сервитута (дата по...)',
            'area_servitut' => 'Площадь сервитута',
            'scheme_servitut' => 'Схема границ сервитута на кадастровом плане территории',
            'report_date' => 'Дата подготовки отчета',
            'pay_servitut' => 'Плата за установление сервитута',
            'pay_period' => 'Период внесения платы',
            'date_preparation' => 'Дата подготовки отчета независимого оценщика',
            'amount_damages' => 'Размер убытков',
            'loss_profits' => 'Упущенная выгода',
            'reclamation_project' => 'Проект рекультивации земель',
            'project_protocol' => 'Проект соглашения об установлении сервитута',
            'project_doc' => 'Проектная документация',
            'reference_objects' => 'Информационная справка об объектах капитального строительства, 
             расположенных на земельном участке',
            'reference_land' => 'Информационная справка об использовании земельного участка',
            'id_app_org' => 'Id организации',
            'cadaster_number' => 'Кадастровый номер',
            'land_address' => 'Адрес, место расположения',
            'land_area' => 'Площадь',
            'id_land' => 'Id земельного участка',
            'other_data_servitut' => 'Другое значение',
            'registr_number' => '',
            'registr_date' => '',
            'customer_registr_date' => '',
            'customer_registr_number' => '',
            'search_status' => 'Выберите статус',
            'search_servitut' => 'Выберите цель севитута',
           // 'search_org' => 'Наименование организации',
            'org_appraiser' => 'Организация оценщик',
            'proprietary_reg_number' => 'Номер документа (вещное право)',
            'proprietary_reg_date' => 'Дата (вещное право)',
            'proprietary_law' => 'вещное право',
            'damages_nds' => 'с НДС / без НДС',
            'loss_nds' => 'с НДС / без НДС',
            'comment' => 'Комментарий',
            'appraiser_registry_number' => 'Регистрационной номер оценщика'
        ];
    }
}
