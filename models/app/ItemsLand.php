<?php

namespace app\models\app;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "items_land".
 *
 * @property int $id
 * @property string $name_land
 */
class ItemsLand extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items_land';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_land'], 'required'],
            [['name_land'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_land' => 'Name Land',
        ];
    }
}
