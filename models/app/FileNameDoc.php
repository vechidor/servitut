<?php

namespace app\models\app;

use Yii;
use app\models\app\file\File;

/**
 * This is the model class for table "file_name_doc".
 *
 * @property int $id
 * @property string $name_doc
 */
class FileNameDoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file_name_doc';
    }
    public function getDoc()
    {
        return $this->hasOne(File::className(), ['title' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_doc'], 'required'],
            [['name_doc'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_doc' => 'Name Doc',
        ];
    }
}
