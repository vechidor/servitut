<?php

namespace app\models;

use app\models\application\Organization;
use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord  implements \yii\web\IdentityInterface
{

    public $password;

    public static function tableName()
    {
        return "user";
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    public function getAuthKey()
    {
        return $this->authKey;
    }
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    public function validatePassword($password)
    {
        if(Yii::$app->getSecurity()->validatePassword($password, $this->password_hash) AND $this->status==10){
            $result_validate = true;
        }else{
            $result_validate = false;
        }
        return $result_validate;
    }

    public function getOrg(){
        return $this->hasOne(Organization::className(), ["id"=>"id_org"]);
    }

    public function attributeLabels()
    {
        return [
            "username"=>"Логин",
            "name"=>"Ф.И.О.",
            "password"=>"Пароль",
            "status"=>"Статус",
            "id_org"=>"Организация"
        ];
    }

    public function rules()
    {
        return [
            [['name','email','password', 'id_org'], 'required'],
            [['username', 'email'], 'email'],
            ['username', 'unique'],
            ['email','unique','message'=>'Пользователь с таким email существует!'],
            ['password', 'string', 'min' => 6],
            ['status', 'integer']
        ];
    }

}
